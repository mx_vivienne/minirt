/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   input2.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/13 01:15:55 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/13 01:15:57 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int	read_double_frac(double *f, char *str, int lineno)
{
	size_t	offset;
	int		power;

	power = 1;
	offset = (*str == '.');
	while (str[offset] != '\0' && offset < 10)
	{
		if (!ft_isdigit(str[offset]))
			return (!put_error_line(ERR_INVDBL, lineno));
		*f = 10 * *f + (str[offset] - '0');
		offset++;
		power *= 10;
	}
	*f /= power;
	return (1);
}

int			read_double(double *d, char *str, int lineno, int mode)
{
	size_t	offset;
	double	frac;
	int		sign;

	*d = 0.0;
	frac = 0.0;
	offset = (*str == '-') || (*str == '+');
	sign = -2 * (*str == '-') + 1;
	if (str[offset] == '\0')
		return (put_error_line(ERR_INVARGS, lineno));
	while (str[offset] != '.' && str[offset] != '\0')
	{
		if (!ft_isdigit(str[offset]))
			return (!put_error_line(ERR_INVDBL, lineno));
		*d = 10 * *d + (str[offset] - '0');
		offset++;
	}
	if (!read_double_frac(&frac, str + offset, lineno))
		return (0);
	*d = sign * (*d + frac);
	return (test_double(*d, lineno, mode));
}

int			read_uint(unsigned int *i, char *str, int lineno, int mode)
{
	size_t	offset;

	offset = 0;
	*i = 0;
	if (*str == '-')
		return (!put_error_line(ERR_NEGINT, lineno));
	if (*str == '+')
		offset++;
	if (str[offset] == '\0')
		return (!put_error_line(ERR_MISSARG, lineno));
	while (str[offset] != '\0')
	{
		if (!ft_isdigit(str[offset]))
			return (!put_error_line(ERR_INVINT, lineno));
		*i = *i * 10 + (str[offset] - '0');
		offset++;
	}
	return (test_uint(*i, lineno, mode));
}

int			test_double(double d, int lineno, int mode)
{
	if (mode & DBLMODE_POS && d < 0.0)
		return (!put_error_line(ERR_NEGDBL, lineno));
	if (mode & DBLMODE_UNIT && d < -1.0)
		return (!put_error_line(ERR_SMALLDBL, lineno));
	if (mode & DBLMODE_UNIT && d > 1.0)
		return (!put_error_line(ERR_LARGEDBL, lineno));
	if (mode & DBLMODE_NOZERO && d == 0.0)
		return (!put_error_line(ERR_ZERODBL, lineno));
	return (1);
}

int			test_uint(unsigned int i, int lineno, int mode)
{
	if (mode & INTMODE_NOZERO && i == 0)
		return (!put_error_line(ERR_ZEROINT, lineno));
	if (mode & INTMODE_BYTE && i > 255)
		return (!put_error_line(ERR_INVBYTE, lineno));
	if (mode & INTMODE_ANGLE && i > 180)
		return (!put_error_line(ERR_INVANGLE, lineno));
	return (1);
}
