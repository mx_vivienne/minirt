/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   misc3.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/09 20:07:59 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/09 20:08:00 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

#if COMPILE_FOR_MAC

int			rt_exit(t_mlx_data *data)
{
	data->current = 0;
	while (data->current < data->len)
	{
		free(data->img[data->current].data);
		data->current++;
	}
	free(data->img);
	objs_destroy(&(data->scene));
	mlx_destroy_image(data->ptrs.mlx, data->ptrs.img.img);
	mlx_destroy_window(data->ptrs.mlx, data->ptrs.win);
	free(data->ptrs.mlx);
	free(data);
	exit(0);
}

#else

int		rt_exit(t_mlx_data *data)
{
	data->current = 0;
	while (data->current < data->len)
	{
		free(data->img[data->current].data);
		data->current++;
	}
	free(data->img);
	objs_destroy(&(data->scene));
	mlx_destroy_image(data->ptrs.mlx, data->ptrs.img.img);
	mlx_destroy_window(data->ptrs.mlx, data->ptrs.win);
	mlx_destroy_display(data->ptrs.mlx);
	free(data->ptrs.mlx);
	free(data);
	exit(0);
}

#endif
