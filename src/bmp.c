/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   bmp.c                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/09 22:54:13 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/10 02:21:34 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static void	str_putint(char *str, unsigned int n)
{
	str[0] = (char)(n & 0xff);
	str[1] = (char)((n >> 8) & 0xff);
	str[2] = (char)((n >> 16) & 0xff);
	str[3] = (char)((n >> 24) & 0xff);
}

static void	put_bmpheader(char *buf, t_pixbuf img, size_t size)
{
	ft_memcpy(buf, "BM----\0\0\0\0\x36\0\0\0\x28\0\0\0--------\x1\0\x18\0"\
			"\0\0\0\0\0\0\0\0\0\10\0\0\0\10\0\0", 46);
	str_putint(buf + 2, (unsigned)size);
	str_putint(buf + 18, (unsigned)img.w);
	str_putint(buf + 22, (unsigned)img.h);
}

static void	put_bmppadding(unsigned char *buf, size_t pad, size_t *pos)
{
	size_t	offset;

	offset = 0;
	while (offset < pad)
	{
		buf[*pos] = 0;
		*pos += 1;
		offset++;
	}
}

static void	put_bmpdata(unsigned char *buf, t_pixbuf img, size_t pad)
{
	size_t	pos;
	size_t	y;
	size_t	x;

	pos = 0;
	y = img.h;
	while (y > 0)
	{
		y--;
		x = 0;
		while (x < img.w)
		{
			buf[pos + 0] = img.data[y * img.w + x].b;
			buf[pos + 1] = img.data[y * img.w + x].g;
			buf[pos + 2] = img.data[y * img.w + x].r;
			pos += 3;
			x++;
		}
		put_bmppadding(buf, pad, &pos);
	}
}

ssize_t		create_bmp(char *file, t_pixbuf img)
{
	int		fd;
	char	*buf;
	size_t	size;
	ssize_t	written;

	size = 3 * img.w;
	if (size % 4 != 0)
		size += 4 - (size % 4);
	size = size * img.h + 54;
	fd = open(file, O_WRONLY | O_CREAT, 0644);
	if (fd < 0)
		return (!put_error(ERR_NEWFILE));
	buf = ft_calloc(size, 1);
	if (buf == NULL)
	{
		close(fd);
		return (!put_error(ERR_ALLOC));
	}
	put_bmpheader(buf, img, size);
	put_bmpdata((unsigned char*)buf + 54, img, (4 - (3 * img.w) % 4) & 3);
	written = write(fd, buf, size);
	close(fd);
	free(buf);
	return (written);
}
