/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   plane.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/14 02:02:49 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/14 02:02:51 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int		pl_put(t_plane pl, t_color c, t_scene *sc)
{
	t_obj		o;
	t_plane		*p;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	p = malloc(sizeof(t_plane));
	if (p == NULL)
		return (!put_error(ERR_ALLOC));
	*p = pl;
	o.type = OBJ_PLANE;
	o.color = c;
	o.obj = (void*)p;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

double	pl_ray(t_ray r, t_plane pl)
{
	double	divisor;

	divisor = v_scalar(r.dir, pl.normal);
	if (fabs(divisor) < EPSILON)
		return (-1.0);
	return (v_scalar(v_subt(pl.origin, r.origin), pl.normal) / divisor);
}

int		pl_read(char **words, t_scene *sc, int lineno)
{
	t_plane	p;
	t_color	c;

	if (ensure_arguments(words, 4, lineno) &&
		v_read(&(p.origin), words[1], lineno, VECMODE_ANY) &&
		v_read(&(p.normal), words[2], lineno, VECMODE_NORM) &&
		c_read(&c, words[3], lineno) &&
		pl_put(p, c, sc))
		return (1);
	return (0);
}
