/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   error.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/22 00:14:39 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/22 00:14:40 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int	put_error(char *msg)
{
	ft_putendl_fd(ERR_ERROR, 2);
	ft_putendl_fd(msg, 2);
	return (1);
}

int	put_error_line(char *msg, int line)
{
	ft_putendl_fd(ERR_ERROR, 2);
	ft_putstr_fd(msg, 2);
	ft_putstr_fd(ERR_LINE, 2);
	ft_putnbr_fd(line, 2);
	ft_putchar_fd('\n', 2);
	return (1);
}
