/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ray_bonus.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/10 02:57:46 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/12 18:53:53 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_vect			r_point(t_ray r, double t)
{
	return (v_add(r.origin, v_factor(r.dir, t)));
}

t_ray			r_through(t_vect orig, t_vect point)
{
	return ((t_ray){orig, v_subt(point, orig)});
}

t_hit			r_trace(t_arr objs, t_ray ray, int do_normal, int do_dist)
{
	size_t	index;
	t_hit	closest;
	double	d;

	index = 0;
	closest = (t_hit){{}, {}, {}, -1.0};
	while (index < objs.len)
	{
		d = obj_ray(ray, objs.arr[index]);
		if (d >= 0 && (d < closest.dist || closest.dist < 0))
			closest = (t_hit){objs.arr[index], {}, {}, d};
		index++;
	}
	if (closest.dist < 0 || !(do_normal || do_dist))
		return (closest);
	closest.point = r_point(ray, closest.dist);
	if (do_normal)
	{
		closest.normal = obj_normal(closest.point, closest.obj);
		if (v_scalar(closest.normal, v_unit(ray.dir)) > 0.0)
			closest.normal = v_factor(closest.normal, -1);
	}
	if (do_dist)
		closest.dist *= v_len(ray.dir);
	return (closest);
}

t_color			r_color(t_scene sc, t_ray ray)
{
	t_hit	hit;

	hit = r_trace(sc.objs, ray, 1, 1);
	if (hit.dist < 0)
		return ((t_color){0, 0, 0});
	return (lighting(sc, hit));
}

double			r_dist(t_ray r, double t)
{
	return (t * v_len(r.dir));
}
