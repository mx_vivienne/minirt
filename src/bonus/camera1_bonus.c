/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   camera1_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/10 18:30:14 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/10 20:17:26 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static t_vect	cam_calc_vectors(t_camera c, t_pixbuf img,
								t_vect *down, t_vect *right)
{
	double	v;
	double	h;

	h = 2 * tan(c.fov / 360.0 * M_PI);
	v = h / img.w * img.h;
	*right = v_unit(v_cross(c.r.dir, c.vup));
	*down = v_factor(v_cross(c.r.dir, *right), v);
	*right = v_factor(*right, h);
	return (v_add4(c.r.origin, c.r.dir, v_div(*down, -2), v_div(*right, -2)));
}

static t_color	cam_calc_pixel(t_camera c, t_scene *sc,
								t_campixel d, t_pixbuf img)
{
	t_color		color;
	size_t		offset;
	double		rx;
	double		ry;

	if (sc->aa == 1)
	{
		return (r_color(*sc, r_through(c.r.origin,
				v_add3(d.corner, v_factor(d.right, (d.x + .5) / img.w),
				v_factor(d.down, (d.y + .5) / img.h)))));
	}
	offset = 0;
	color = (t_color){0, 0, 0};
	while (offset < sc->aa)
	{
		rx = (double)rand() / RAND_MAX;
		ry = (double)rand() / RAND_MAX;
		color = c_add_uncapped(color, r_color(*sc, r_through(c.r.origin,
					v_add3(d.corner, v_factor(d.right, (d.x + rx) / img.w),
						v_factor(d.down, (d.y + ry) / img.h)))));
		offset++;
	}
	return (c_div(color, sc->aa));
}

void			cam_render_one(t_camera c, t_scene *sc, t_pixbuf img,
					t_thrinfo thr)
{
	if (c.anaglyph == 0)
		cam_render(c, sc, img, thr);
	else
		cam3d_render(c, sc, img, thr);
}

void			cam_render(t_camera c, t_scene *sc, t_pixbuf img, t_thrinfo thr)
{
	t_vect	corner;
	size_t	x;
	size_t	y;
	t_vect	down;
	t_vect	right;

	corner = cam_calc_vectors(c, img, &down, &right);
	y = thr.num;
	while (y < img.h)
	{
		x = 0;
		while (x < img.w)
		{
			img.data[y * img.w + x] = filter_apply(cam_calc_pixel(c, sc,
				(t_campixel){x, y, corner, down, right}, img), c.f, x, y);
			x++;
		}
		y += thr.total;
	}
}

void			cam3d_render(t_camera c, t_scene *sc, t_pixbuf img,
					t_thrinfo thr)
{
	t_campixel	p;
	t_color		*co;
	t_camera	*eyes;

	p.corner = cam_calc_vectors(c, img, &(p.down), &(p.right));
	eyes = (t_camera[]){c, c};
	eyes[0].r.origin = v_add(c.r.origin,
		v_factor(v_unit(v_cross(c.vup, c.r.dir)), c.anaglyph));
	eyes[1].r.origin = v_add(c.r.origin,
		v_factor(v_unit(v_cross(c.r.dir, c.vup)), c.anaglyph));
	p.y = thr.num;
	while (p.y < img.h)
	{
		p.x = 0;
		while (p.x < img.w)
		{
			co = (t_color[]){cam_calc_pixel(eyes[0], sc, p, img),
				cam_calc_pixel(eyes[1], sc, p, img)};
			img.data[p.y * img.w + p.x] = filter_apply(c_add((t_color){
				co[0].r, 0, 0}, (t_color){0, co[1].g, co[1].b}), c.f, p.x, p.y);
			p.x++;
		}
		p.y += thr.total;
	}
}
