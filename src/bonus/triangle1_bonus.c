/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   triangle1_bonus.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/20 00:53:25 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/20 00:53:27 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static t_square	tr_bounding_square(t_tri tr)
{
	t_square	sq;
	t_vect		ab;
	t_vect		ac;
	double		left;

	ab = v_subt(tr.b, tr.a);
	ac = v_subt(tr.c, tr.a);
	sq.right = tr.right;
	sq.up = v_unit(v_cross(tr.normal, tr.right));
	sq.normal = tr.normal;
	left = fmin(0, v_scalar(ac, sq.right));
	sq.size = fmax(v_scalar(ab, sq.right) - left, v_scalar(ac, sq.up));
	sq.center = v_add3(tr.a, v_factor(sq.right, sq.size / 2 + left),
		v_factor(sq.up, sq.size / 2));
	return (sq);
}

int				tr_put(t_tri tr, t_material mat, t_scene *sc)
{
	t_obj	o;
	t_tri	*t;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	t = malloc(sizeof(t_tri));
	if (t == NULL)
		return (!put_error(ERR_ALLOC));
	tr.up = v_unit(v_subt(tr.c, tr.a));
	tr.right = v_unit(v_subt(tr.b, tr.a));
	tr.normal = v_unit(v_cross(tr.right, tr.up));
	tr.bounds = tr_bounding_square(tr);
	*t = tr;
	o.type = OBJ_TRIANGLE;
	o.mat = mat;
	o.obj = (void*)t;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

double			tr_ray(t_ray r, t_tri t)
{
	double	divisor;
	double	d;
	t_vect	p;

	divisor = v_scalar(r.dir, t.normal);
	if (fabs(divisor) < EPSILON)
		return (-1.0);
	d = v_scalar(v_subt(t.a, r.origin), t.normal) / divisor;
	if (d < 0.0)
		return (-1.0);
	p = r_point(r, d);
	if (v_scalar(t.normal, v_cross(v_subt(t.b, t.a), v_subt(p, t.a))) < 0 ||
		v_scalar(t.normal, v_cross(v_subt(t.c, t.b), v_subt(p, t.b))) < 0 ||
		v_scalar(t.normal, v_cross(v_subt(t.a, t.c), v_subt(p, t.c))) < 0)
		return (-1.0);
	return (d);
}

int				tr_read(char **words, t_scene *sc, int lineno)
{
	t_tri		tr;
	t_material	mat;

	mat = (t_material){0};
	if (ensure_arguments(words, 5, lineno) &&
		v_read(&(tr.a), words[1], lineno, VECMODE_ANY) &&
		v_read(&(tr.b), words[2], lineno, VECMODE_ANY) &&
		v_read(&(tr.c), words[3], lineno, VECMODE_ANY) &&
		m_name_read(&mat, words[4], *sc, lineno) &&
		tr_put(tr, mat, sc))
		return (1);
	return (0);
}
