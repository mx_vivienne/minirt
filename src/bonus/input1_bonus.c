/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   input1_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/07 22:21:30 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/07 22:24:15 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static unsigned int	g_mandatory_objs[] = {
	OBJ_RESOLUTION, OBJ_AMBIENT, OBJ_CAMERA, 0};

static unsigned int	g_unique_objs[] = {
	OBJ_RESOLUTION, OBJ_AMBIENT, OBJ_ANTIALIAS, OBJ_THREADS, OBJ_SKYBOX, 0};

static int			input_read_loop(int fd, t_scene *sc, int *flags)
{
	int		ret;
	char	*line;
	char	**words;

	ret = get_next_line(fd, &line);
	while (ret > 0)
	{
		if (ft_isinset(line, '#'))
			*ft_strchr(line, '#') = '\0';
		words = ft_split(line, ' ');
		free(line);
		if (!line_read(words, sc, flags))
		{
			objs_destroy(sc);
			return (0);
		}
		ret = get_next_line(fd, &line);
	}
	if (ret == 0)
	{
		free(line);
		return (1);
	}
	objs_destroy(sc);
	return (!put_error(ERR_FILEREAD));
}

int					input_read(int fd, t_scene *sc)
{
	int	flags;

	flags = 0;
	if (fd < 0)
		return (!put_error(ERR_FILEREAD));
	flags = 0;
	if (!input_read_loop(fd, sc, &flags))
		return (0);
	if (!(flags & 1))
		return (!put_error(ERR_NORES));
	if (!(flags & 2))
		return (!put_error(ERR_NOAMB));
	if (!(flags & 4))
		return (!put_error(ERR_NOCAMERA));
	return (1);
}

static int			line_read_flags(unsigned int code, int *flags, int lineno)
{
	size_t			offset;

	offset = 0;
	while (g_mandatory_objs[offset] != 0)
	{
		if (code == g_mandatory_objs[offset])
		{
			*flags |= 1U << offset;
			break ;
		}
		offset++;
	}
	offset = 0;
	while (g_unique_objs[offset] != 0)
	{
		if (code == g_unique_objs[offset])
		{
			if (*flags & 1U << (16 + offset))
				return (!put_error_line(ERR_DUPINFO, lineno));
			*flags |= 1U << (16 + offset);
			break ;
		}
		offset++;
	}
	return (1);
}

int					line_read(char **words, t_scene *sc, int *flags)
{
	static int	lineno = 1;
	int			ret;

	if (words == NULL)
		return (!put_error_line(ERR_LINEREAD, lineno));
	ret = 1;
	if (*words != NULL)
	{
		ret = obj_read(words, sc, lineno) &&
			line_read_flags(encode_str(words[0]), flags, lineno);
	}
	words_destroy(&words);
	lineno++;
	return (ret);
}

void				words_destroy(char ***words)
{
	size_t	count;

	if (words == NULL || *words == NULL)
		return ;
	count = 0;
	while ((*words)[count] != NULL)
	{
		free((*words)[count]);
		count++;
	}
	free(*words);
	*words = NULL;
}
