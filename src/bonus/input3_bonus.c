/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   input3_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/14 03:19:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/14 03:19:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int		test_double(double d, int lineno, int mode)
{
	if (mode & DBLMODE_POS && d < 0.0)
		return (!put_error_line(ERR_NEGDBL, lineno));
	if (mode & DBLMODE_UNIT && d < -1.0)
		return (!put_error_line(ERR_SMALLDBL, lineno));
	if (mode & DBLMODE_UNIT && d > 1.0)
		return (!put_error_line(ERR_LARGEDBL, lineno));
	if (mode & DBLMODE_NOZERO && d == 0.0)
		return (!put_error_line(ERR_ZERODBL, lineno));
	return (1);
}

int		test_uint(unsigned int i, int lineno, int mode)
{
	if (mode & INTMODE_NOZERO && i == 0)
		return (!put_error_line(ERR_ZEROINT, lineno));
	if (mode & INTMODE_BYTE && i > 255)
		return (!put_error_line(ERR_INVBYTE, lineno));
	if (mode & INTMODE_ANGLE && i > 180)
		return (!put_error_line(ERR_INVANGLE, lineno));
	return (1);
}

int		amb_read(char **words, t_scene *sc, int lineno)
{
	double	intensity;

	if (ensure_arguments(words, 3, lineno) &&
		read_double(&intensity, words[1], lineno, DBLMODE_POS | DBLMODE_UNIT) &&
		c_read(&(sc->amb), words[2], lineno))
	{
		sc->amb = c_shade(sc->amb, intensity);
		return (1);
	}
	return (0);
}

int		res_read(char **words, t_scene *sc, int lineno)
{
	if (ensure_arguments(words, 3, lineno) &&
		read_uint((unsigned*)&(sc->x), words[1], lineno, INTMODE_NOZERO) &&
		read_uint((unsigned*)&(sc->y), words[2], lineno, INTMODE_NOZERO))
		return (1);
	return (0);
}

int		aa_read(char **words, t_scene *sc, int lineno)
{
	if (ensure_arguments(words, 2, lineno) &&
		read_uint((unsigned*)&(sc->aa), words[1], lineno, INTMODE_NOZERO))
		return (1);
	return (0);
}
