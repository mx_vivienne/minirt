/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   triangle2_bonus.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/31 19:07:07 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/31 19:07:09 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_vect			tr_normal(t_vect p, t_obj o)
{
	t_tri	tr;

	tr = *(t_tri*)o.obj;
	if (o.mat.surface.type == M_SURF_REGULAR)
		return (tr.normal);
	o.obj = &(tr.bounds);
	return (sq_normal(p, o));
}

t_coords		tr_uv(t_vect p, t_obj o)
{
	t_tri	tr;

	tr = *(t_tri*)(o.obj);
	o.obj = (void*)&(tr.bounds);
	return (sq_uv(p, o));
}
