/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   plane_bonus.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/14 02:02:49 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/02/07 23:01:26 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int			pl_put(t_plane pl, t_material mat, t_scene *sc)
{
	t_obj		o;
	t_plane		*p;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	p = malloc(sizeof(t_plane));
	if (p == NULL)
		return (!put_error(ERR_ALLOC));
	if (v_exact_equals(pl.up, (t_vect){0, 0, 0}))
		pl.up = (t_vect){0, 1, 0};
	if (fabs(v_scalar(pl.up, pl.normal)) > 1.0 - EPSILON)
		pl.up = (t_vect){0, 0, -1};
	pl.right = v_unit(v_cross(pl.up, pl.normal));
	pl.up = v_unit(v_cross(pl.normal, pl.right));
	*p = pl;
	o.type = OBJ_PLANE;
	o.mat = mat;
	o.obj = (void*)p;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

double		pl_ray(t_ray r, t_plane pl)
{
	double	divisor;

	divisor = v_scalar(r.dir, pl.normal);
	if (fabs(divisor) < EPSILON)
		return (-1.0);
	return (v_scalar(v_subt(pl.origin, r.origin), pl.normal) / divisor);
}

int			pl_read(char **words, t_scene *sc, int lineno)
{
	t_plane		p;
	t_material	mat;

	p = (t_plane){(t_vect){0, 0, 0}, (t_vect){0, 0, 0},
		(t_vect){0, 0, 0}, (t_vect){0, 0, 0}};
	if (ensure_arguments(words, 4, lineno) &&
		v_read(&(p.origin), words[1], lineno, VECMODE_ANY) &&
		v_read(&(p.normal), words[2], lineno, VECMODE_NORM) &&
		m_name_read(&mat, words[3], *sc, lineno) &&
		pl_put(p, mat, sc))
		return (1);
	return (0);
}

t_vect		pl_normal(t_vect p, t_obj o)
{
	t_plane		pl;
	double		angle;

	pl = *(t_plane*)o.obj;
	if (o.mat.surface.type != M_SURF_REGULAR)
	{
		if (o.mat.surface.type == M_SURF_SINE)
		{
			angle = .5 * sin(M_PI * v_scalar(p, pl.up));
			return (v_rotate(pl.normal, pl.right, angle));
		}
	}
	return (pl.normal);
}

t_coords	pl_uv(t_vect p, t_obj o)
{
	t_vect		vect;
	t_plane		pl;

	pl = *(t_plane*)o.obj;
	vect = v_subt(p, pl.origin);
	return ((t_coords){
		v_scalar(vect, pl.right),
		-v_scalar(vect, pl.up)
	});
}
