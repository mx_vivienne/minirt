/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   filter_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/17 01:50:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/17 01:50:56 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static t_matrix		g_filter_sepia = {
	(t_vect){.393, .349, .272},
	(t_vect){.769, .686, .534},
	(t_vect){.189, .168, .131}
};

static t_matrix		g_filter_greyscale = {
	(t_vect){.2126, .2126, .2126},
	(t_vect){.7152, .7152, .7152},
	(t_vect){.0722, .0722, .0722}
};

t_color				filter_hue_rotate(t_color c, double angle)
{
	double	h;
	double	v;
	double	ch;
	double	*cl;

	if (c.r == c.g && c.g == c.b)
		return (c);
	cl = (double[3]){c.r / 255.0, c.g / 255.0, c.b / 255.0};
	v = fmax(fmax(cl[0], cl[1]), cl[2]);
	ch = v - fmin(fmin(cl[0], cl[1]), cl[2]);
	if (v == cl[0])
		h = fmod(0 + (cl[1] - cl[2]) / ch + angle / 60, 60);
	else if (v == cl[1])
		h = fmod(2 + (cl[2] - cl[0]) / ch + angle / 60, 60);
	else
		h = fmod(4 + (cl[0] - cl[1]) / ch + angle / 60, 60);
	cl = (double[3]){fmod(5 + h, 6), fmod(3 + h, 6), fmod(1 + h, 6)};
	return ((t_color){
		(unsigned)(255 * (v - ch * fmax(0, fmin(fmin(cl[0], 4 - cl[0]), 1)))),
		(unsigned)(255 * (v - ch * fmax(0, fmin(fmin(cl[1], 4 - cl[1]), 1)))),
		(unsigned)(255 * (v - ch * fmax(0, fmin(fmin(cl[2], 4 - cl[2]), 1))))
	});
}

t_color				filter_apply(t_color c, t_filter f, size_t x, size_t y)
{
	t_matrix	mtx;
	t_vect		v;

	(void)x;
	if (f.type == FILTER_SEPIA)
		mtx = g_filter_sepia;
	else if (f.type == FILTER_GREYSCL)
		mtx = g_filter_greyscale;
	else if (f.type == FILTER_INVERT)
		return ((t_color){255 - c.r, 255 - c.g, 255 - c.b});
	else if (f.type == FILTER_ROT_HUE)
		return (filter_hue_rotate(c, f.args[0]));
	else if (f.type == FILTER_SCNLINE)
	{
		if (y % 2)
			return (c_shade(c, fmin(1, fmax(0, 1 - f.args[0]))));
		return (c);
	}
	else
		return (c);
	v = m_apply(mtx, (t_vect){c.r, c.g, c.b});
	return (c_cap((t_color){(unsigned)v.x, (unsigned)v.y, (unsigned)v.z}));
}
