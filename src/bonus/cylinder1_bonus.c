/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   cylinder1_bonus.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/21 18:40:09 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/21 18:40:11 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static t_matrix	cy_affine_matrix(t_cyl cy)
{
	t_matrix	mtx;

	mtx = rotation_matrix(cy.up, (t_vect){0, 1, 0});
	mtx = m_mult(
		stretch_matrix((t_vect){1 / cy.radius, 1 / cy.size, 1 / cy.radius}),
		mtx);
	return (mtx);
}

int				cy_put(t_cyl cy, t_material mat, t_scene *sc, int capped)
{
	t_cyl	*cyl;

	if (capped)
	{
		if (!ci_put((t_circle){v_add(cy.center, v_factor(cy.up, cy.size / 2)),
				cy.up, {0, 0, 0}, {0, 0, 0}, cy.radius}, mat, sc) ||
			!ci_put((t_circle){v_add(cy.center, v_factor(cy.up, -.5 * cy.size)),
				v_factor(cy.up, -1), {0, 0, 0}, {0, 0, 0}, cy.radius}, mat, sc))
			return (0);
	}
	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	cyl = malloc(sizeof(t_cyl));
	if (cyl == NULL)
		return (!put_error(ERR_ALLOC));
	cy.right = (t_vect){0, 0, 1};
	if (fabs(v_scalar(cy.right, cy.up)) > 1.0 - EPSILON)
		cy.right = (t_vect){1, 0, 0};
	cy.radius /= 2;
	cy.size /= 2;
	*cyl = (t_cyl){cy.center, cy.up, v_unit(v_cross(cy.right, cy.up)),
		cy.radius, cy.size, cy_affine_matrix(cy)};
	sc->objs.arr[sc->objs.len] = (t_obj){OBJ_CYLINDER, mat, (void*)cyl};
	sc->objs.len += 1;
	return (1);
}

double			cy_ray(t_ray r, t_cyl cy)
{
	t_vect	f;
	double	disc;

	r = (t_ray){m_apply(cy.mtx, v_subt(r.origin, cy.center)),
		m_apply(cy.mtx, r.dir)};
	f.x = r.dir.x * r.dir.x + r.dir.z * r.dir.z;
	f.y = 2 * (r.origin.x * r.dir.x + r.origin.z * r.dir.z);
	f.z = r.origin.x * r.origin.x + r.origin.z * r.origin.z - 1.0;
	disc = f.y * f.y - 4.0 * f.x * f.z;
	if (disc < 0)
		return (-1.0);
	disc = sqrt(disc);
	f.x *= 2;
	return (test_hits(r, (-f.y + disc) / f.x, (-f.y - disc) / f.x));
}

int				cy_read(char **words, t_scene *sc, int lineno)
{
	t_cyl		cy;
	t_material	mat;
	int			capped;

	capped = 0;
	if (ensure_args_range(words, 6, 7, lineno) &&
		v_read(&(cy.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(cy.up), words[2], lineno, VECMODE_NORM) &&
		read_double(&(cy.radius), words[3], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		read_double(&(cy.size), words[4], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		m_name_read(&mat, words[5], *sc, lineno))
	{
		if (words[6] != NULL)
		{
			if (0 != ft_strncmp(words[6], "capped", 7))
				return (!put_error_line(ERR_KEYWORD, lineno));
			capped = 1;
		}
		return (cy_put(cy, mat, sc, capped));
	}
	return (0);
}
