/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   sphere_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/12 20:07:48 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/12 20:07:50 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int			sp_put(t_sphere sp, t_material mat, t_scene *sc)
{
	t_obj		o;
	t_sphere	*s;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	s = malloc(sizeof(t_sphere));
	if (s == NULL)
		return (!put_error(ERR_ALLOC));
	sp.radius /= 2;
	*s = sp;
	o.type = OBJ_SPHERE;
	o.mat = mat;
	o.obj = (void*)s;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

double		sp_ray(t_ray r, t_sphere s)
{
	t_vect	oc;
	t_vect	f;
	double	disc;

	oc = v_subt(r.origin, s.center);
	f.x = v_scalar(r.dir, r.dir);
	f.y = v_scalar(r.dir, oc);
	f.z = v_scalar(oc, oc) - s.radius * s.radius;
	disc = f.y * f.y - f.x * f.z;
	if (disc < 0)
		return (-1.0);
	disc = sqrt(disc);
	if (-f.y - disc < 0)
		return ((-f.y + disc) / f.x);
	return ((-f.y - disc) / f.x);
}

int			sp_read(char **words, t_scene *sc, int lineno)
{
	t_sphere	sp;
	t_material	mat;

	mat = (t_material){0};
	if (ensure_arguments(words, 4, lineno) &&
		v_read(&(sp.center), words[1], lineno, VECMODE_ANY) &&
		read_double(&(sp.radius), words[2], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		m_name_read(&mat, words[3], *sc, lineno) &&
		sp_put(sp, mat, sc))
		return (1);
	return (0);
}

t_vect		sp_normal(t_vect p, t_obj o)
{
	t_sphere	sp;
	t_vect		normal;
	t_vect		cw;
	double		angle;

	sp = *(t_sphere*)o.obj;
	normal = v_unit(v_subt(p, sp.center));
	if (o.mat.surface.type != M_SURF_REGULAR)
	{
		cw = v_unit(v_cross((t_vect){0, 1, 0}, normal));
		if (o.mat.surface.type == M_SURF_SINE)
		{
			angle = sin(10 * M_PI * (1 - v_scalar(normal, (t_vect){0, 1, 0})));
			return (v_rotate(normal, cw, .5 * angle));
		}
	}
	return (normal);
}

t_coords	sp_uv(t_vect p, t_obj o)
{
	t_coords	uv;
	t_sphere	sp;

	sp = *(t_sphere*)o.obj;
	p = v_unit(v_subt(p, sp.center));
	uv.x = .5 + (atan2(p.x, p.z)) / (2 * M_PI);
	uv.y = .5 - asin(p.y) / M_PI;
	return (uv);
}
