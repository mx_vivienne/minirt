/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   square_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/20 01:10:08 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/20 01:10:09 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int			sq_put(t_square sq, t_material mat, t_scene *sc, int is_skybox)
{
	t_obj		o;
	t_square	*s;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	s = malloc(sizeof(t_square));
	if (s == NULL)
		return (!put_error(ERR_ALLOC));
	if (v_exact_equals(sq.up, (t_vect){0, 0, 0}))
		sq.up = (t_vect){0, 1, 0};
	if (fabs(v_scalar(sq.up, sq.normal)) > 1.0 - EPSILON)
		sq.up = (t_vect){0, 0, -1};
	sq.right = v_unit(v_cross(sq.up, sq.normal));
	sq.up = v_unit(v_cross(sq.normal, sq.right));
	*s = sq;
	o.type = OBJ_SQUARE;
	if (is_skybox)
		o.type = OBJ_SKYBOX;
	o.mat = mat;
	o.obj = (void*)s;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

double		sq_ray(t_ray r, t_square sq)
{
	double	divisor;
	double	d;
	t_vect	point;

	divisor = v_scalar(r.dir, sq.normal);
	if (fabs(divisor) < EPSILON)
		return (-1.0);
	d = v_scalar(v_subt(sq.center, r.origin), sq.normal) / divisor;
	if (d < 0.0)
		return (-1.0);
	point = v_subt(r_point(r, d), sq.center);
	if (fabs(v_scalar(point, sq.right)) <= sq.size / 2 &&
		fabs(v_scalar(point, sq.up)) <= sq.size / 2)
		return (d);
	return (-1.0);
}

int			sq_read(char **words, t_scene *sc, int lineno)
{
	t_square	sq;
	t_material	mat;

	sq.up = (t_vect){0, 0, 0};
	if (ensure_arguments(words, 5, lineno) &&
		v_read(&(sq.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(sq.normal), words[2], lineno, VECMODE_NORM) &&
		read_double(&(sq.size), words[3], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		m_name_read(&mat, words[4], *sc, lineno) &&
		sq_put(sq, mat, sc, 0))
		return (1);
	return (0);
}

t_vect		sq_normal(t_vect p, t_obj o)
{
	t_square	sq;
	double		angle;

	sq = *(t_square*)o.obj;
	if (o.mat.surface.type != M_SURF_REGULAR)
	{
		if (o.mat.surface.type == M_SURF_SINE)
		{
			angle = .5 * sin(10 * M_PI * v_scalar(p, sq.up) / sq.size);
			return (v_rotate(sq.normal, sq.right, angle));
		}
	}
	return (sq.normal);
}

t_coords	sq_uv(t_vect p, t_obj o)
{
	t_vect		vect;
	t_square	sq;

	sq = *(t_square*)o.obj;
	vect = v_subt(p, sq.center);
	return (t_coords){
		(sq.size / 2 + v_scalar(vect, sq.right)) / sq.size,
		(sq.size / 2 - v_scalar(vect, sq.up)) / sq.size
	};
}
