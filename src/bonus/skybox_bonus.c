/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   skybox_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/28 05:52:02 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/28 05:52:03 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static int	sky_square(t_vect center, double size, t_material mat, t_scene *sc)
{
	return (sq_put((t_square){center, v_factor(center, -1), (t_vect){0, 0, 0},
		(t_vect){0, 0, 0}, size}, mat, sc, 1));
}

int			sky_put(double size, t_material *mats, t_scene *sc)
{
	double	radius;

	radius = size / 2;
	if (!sky_square((t_vect){radius, 0, 0}, size, mats[0], sc) ||
		!sky_square((t_vect){-radius, 0, 0}, size, mats[0], sc) ||
		!sky_square((t_vect){0, radius, 0}, size, mats[0], sc) ||
		!sky_square((t_vect){0, -radius, 0}, size, mats[0], sc) ||
		!sky_square((t_vect){0, 0, radius}, size, mats[0], sc) ||
		!sky_square((t_vect){0, 0, -radius}, size, mats[0], sc))
		return (0);
	return (1);
}

int			sky_read(char **words, t_scene *sc, int lineno)
{
	double		size;
	t_material	mats[6];

	if (ensure_arguments(words, 8, lineno) &&
		read_double(&size, words[1], lineno, DBLMODE_NOZERO | DBLMODE_POS) &&
		m_name_read(mats, words[2], *sc, lineno) &&
		m_name_read(mats, words[3], *sc, lineno) &&
		m_name_read(mats, words[4], *sc, lineno) &&
		m_name_read(mats, words[5], *sc, lineno) &&
		m_name_read(mats, words[6], *sc, lineno) &&
		m_name_read(mats, words[7], *sc, lineno) &&
		sky_put(size, mats, sc))
		return (1);
	return (0);
}
