/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   objs_bonus.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/16 17:26:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/16 17:26:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static unsigned int	g_types[] = {
	OBJ_SPHERE, OBJ_CYLINDER, OBJ_CUBE, OBJ_PYRAMID, OBJ_CONE,
	OBJ_TRIANGLE, OBJ_SQUARE, OBJ_PLANE, OBJ_CIRCLE, OBJ_SKYBOX,
	OBJ_LIGHT, OBJ_LIGHT2, OBJ_CAMERA, OBJ_AMBIENT, OBJ_RESOLUTION,
	OBJ_ANTIALIAS, OBJ_THREADS, OBJ_MATERIAL, 0
};

static int			(*g_readfuncs[])(char**, t_scene*, int) = {
	sp_read, cy_read, cb_read, py_read, co_read,
	tr_read, sq_read, pl_read, ci_read, sky_read,
	l_read, l2_read, cam_read, amb_read, res_read,
	aa_read, th_read, mat_read, NULL
};

static t_coords		(*g_uvfuncs[])(t_vect, t_obj) = {
	sp_uv, cy_uv, NULL, NULL, co_uv,
	tr_uv, sq_uv, pl_uv, ci_uv, sq_uv,
	NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL
};

int					obj_read(char **words, t_scene *sc, int ln)
{
	unsigned int	type;
	size_t			offset;

	offset = 0;
	type = encode_str(words[0]);
	while (g_types[offset] != 0)
	{
		if (type == g_types[offset] && g_readfuncs[offset] != NULL)
			return ((g_readfuncs[offset](words, sc, ln)));
		offset++;
	}
	return (!put_error_line(ERR_INVTYPE, ln));
}

double				obj_ray(t_ray r, t_obj o)
{
	if (o.type == OBJ_SPHERE)
		return (sp_ray(r, *(t_sphere*)o.obj));
	if (o.type == OBJ_CYLINDER)
		return (cy_ray(r, *(t_cyl*)o.obj));
	if (o.type == OBJ_CONE)
		return (co_ray(r, *(t_cone*)o.obj));
	if (o.type == OBJ_TRIANGLE)
		return (tr_ray(r, *(t_tri*)o.obj));
	if (o.type == OBJ_SQUARE || o.type == OBJ_SKYBOX)
		return (sq_ray(r, *(t_square*)o.obj));
	if (o.type == OBJ_CIRCLE)
		return (ci_ray(r, *(t_circle*)o.obj));
	if (o.type == OBJ_PLANE)
		return (pl_ray(r, *(t_plane*)o.obj));
	return (-1.0);
}

t_vect				obj_normal(t_vect p, t_obj o)
{
	t_vect	normal;

	normal = (t_vect){0, 0, 0};
	if (o.type == OBJ_SPHERE)
		normal = sp_normal(p, o);
	else if (o.type == OBJ_TRIANGLE)
		normal = tr_normal(p, o);
	else if (o.type == OBJ_SQUARE || o.type == OBJ_SKYBOX)
		normal = sq_normal(p, o);
	else if (o.type == OBJ_CIRCLE)
		normal = ci_normal(p, o);
	else if (o.type == OBJ_PLANE)
		normal = pl_normal(p, o);
	else if (o.type == OBJ_CYLINDER)
		normal = cy_normal(p, o);
	else if (o.type == OBJ_CONE)
		normal = co_normal(p, o);
	return (normal);
}



t_coords			obj_uv(t_vect p, t_obj o)
{
	size_t	offset;

	offset = 0;
	while (g_types[offset] != 0)
	{
		if (g_types[offset] == o.type)
		{
			if (g_uvfuncs[offset] != NULL)
				return (g_uvfuncs[offset](p, o));
			else
				break ;
		}
		offset++;
	}
	return ((t_coords){0, 0});
}
