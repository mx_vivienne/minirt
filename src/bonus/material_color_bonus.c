/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   material_color_bonus.c                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/20 03:11:32 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/20 03:11:34 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static int	g_max_len = 8;

static char	*g_color_keywords[] = {
	"flat", "checker", "rainbow", "rgb", "texture", NULL
};

static int	mat_color_img(t_property *col, char **words, int lineno)
{
	t_pixbuf	*img;
	int			fd;

	if (!ensure_arguments(words, 2, lineno))
		return (0);
	fd = open(words[1], O_RDONLY);
	if (fd < 0)
		return (!put_error_line(ERR_IMG_READ, lineno));
	img = malloc(sizeof(t_pixbuf));
	if (img == NULL)
	{
		close(fd);
		return (!put_error(ERR_ALLOC));
	}
	if (read_ppm(img, fd, lineno))
	{
		close(fd);
		col->data = img;
		return (1);
	}
	close(fd);
	free(img);
	return (0);
}

static int	mat_color_rgb(t_property *c, int amount, char **words, int ln)
{
	int	offset;

	if (!ensure_args_range(words, amount + 1, amount + 2, ln))
		return (0);
	if (amount > 0)
	{
		c->data = malloc(amount * sizeof(t_color));
		if (c->data == NULL)
			return (!put_error(ERR_ALLOC));
		offset = 0;
		while (offset < amount)
		{
			if (!c_read((t_color*)(c->data) + offset, words[offset + 1], ln))
				return (0);
			offset++;
		}
	}
	if (words[amount + 1] != NULL)
	{
		return (read_double(&(c->scale), words[amount + 1],
			ln, DBLMODE_NOZERO));
	}
	return (1);
}

static int	mat_color_args(t_property *col, char **words, int lineno)
{
	col->data = NULL;
	col->scale = 1;
	if (col->type == M_COL_RGB)
		return (mat_color_rgb(col, 0, words, lineno));
	if (col->type == M_COL_FLAT || col->type == M_COL_RAINBOW)
		return (mat_color_rgb(col, 1, words, lineno));
	else if (col->type == M_COL_CHECKER)
		return (mat_color_rgb(col, 2, words, lineno));
	else if (col->type == M_COL_IMAGE)
		return (mat_color_img(col, words, lineno));
	return (0);
}

static int	mat_color_type(t_property *col, char **words, int lineno)
{
	size_t	offset;

	offset = 0;
	while (1)
	{
		if (g_color_keywords[offset] == NULL)
			return (!put_error_options(ERR_KEYWORD, lineno, g_color_keywords));
		if (!ft_strncmp(words[0], g_color_keywords[offset], g_max_len))
		{
			col->type = offset;
			return (1);
		}
		offset++;
	}
}

int			mat_read_color(t_property *col, char *params, int lineno)
{
	int		ret;
	char	**words;

	words = ft_split(params, ':');
	if (words[0] == NULL)
	{
		words_destroy(&words);
		return (!put_error_line(ERR_FEWARGS, lineno));
	}
	ret = mat_color_type(col, words, lineno) &&
		mat_color_args(col, words, lineno);
	words_destroy(&words);
	return (ret);
}
