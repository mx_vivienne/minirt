/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   circle_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/27 16:27:36 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/27 16:27:40 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int				ci_put(t_circle ci, t_material mat, t_scene *sc)
{
	t_obj		o;
	t_circle	*circ;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	circ = malloc(sizeof(t_circle));
	if (circ == NULL)
		return (!put_error(ERR_ALLOC));
	ci.radius /= 2;
	if (v_exact_equals(ci.up, (t_vect){0, 0, 0}))
		ci.up = (t_vect){0, 1, 0};
	if (fabs(v_scalar(ci.up, ci.normal)) > 1.0 - EPSILON)
		ci.up = (t_vect){0, 0, -1};
	ci.right = v_unit(v_cross(ci.up, ci.normal));
	ci.up = v_unit(v_cross(ci.normal, ci.right));
	*circ = ci;
	o.type = OBJ_CIRCLE;
	o.mat = mat;
	o.obj = (void*)circ;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

double			ci_ray(t_ray r, t_circle ci)
{
	double	d;

	d = v_scalar(r.dir, ci.normal);
	if (fabs(d) < EPSILON)
		return (-1.0);
	d = v_scalar(v_subt(ci.center, r.origin), ci.normal) / d;
	if (d >= 0.0 && v_len(v_subt(r_point(r, d), ci.center)) <= ci.radius)
		return (d);
	return (-1.0);
}

int				ci_read(char **words, t_scene *sc, int lineno)
{
	t_circle	ci;
	t_material	mat;

	mat = (t_material){0};
	if (ensure_arguments(words, 5, lineno) &&
		v_read(&(ci.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(ci.normal), words[2], lineno, VECMODE_NORM) &&
		read_double(&(ci.radius), words[3], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		m_name_read(&mat, words[4], *sc, lineno) &&
		ci_put(ci, mat, sc))
		return (1);
	return (0);
}

t_vect			ci_normal(t_vect p, t_obj o)
{
	t_circle	ci;
	double		angle;

	ci = *(t_circle*)o.obj;
	if (o.mat.surface.type != M_SURF_REGULAR)
	{
		if (o.mat.surface.type == M_SURF_SINE)
		{
			angle = .5 * sin(5 * M_PI * v_scalar(p, ci.up) / ci.radius);
			return (v_rotate(ci.normal, ci.right, angle));
		}
	}
	return (ci.normal);
}

t_coords		ci_uv(t_vect p, t_obj o)
{
	t_vect		vect;
	t_circle	ci;

	ci = *(t_circle*)o.obj;
	vect = v_subt(p, ci.center);
	return ((t_coords){
		(ci.radius + v_scalar(vect, ci.right)) / 2 / ci.radius,
		(ci.radius - v_scalar(vect, ci.up)) / 2 / ci.radius
	});
}
