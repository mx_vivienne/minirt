/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   threads_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/02/01 22:41:49 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/02/01 22:41:50 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

long	render_threaded(t_scene *sc, t_pixbuf **img)
{
	pthread_t	subthread;
	t_thrarg	args;
	void		*ret;

	if (!img_alloc(*sc, img))
		return (0);
	ret = (void*)1;
	if (sc->threads > 1)
	{
		args.scene = sc;
		args.img = img;
		args.thread = (t_thrinfo){0, sc->threads};
		if (pthread_attr_init(&(args.attr)))
			return (!put_error(ERR_THRINIT));
		pthread_attr_setdetachstate(&(args.attr), PTHREAD_CREATE_JOINABLE);
		if (!pthread_create(&subthread, &(args.attr), &thread_start, &args))
			pthread_join(subthread, &ret);
		else
			put_error(ERR_THRINIT);
		pthread_attr_destroy(&(args.attr));
		return ((long)ret);
	}
	return (cam_render_all(sc, img, (t_thrinfo){0, 1}));
}

void	*thread_start(void *ptr)
{
	t_thrarg	args;
	t_thrarg	next;
	pthread_t	subthread;
	void		*ret;
	int			ret2;

	ret = (void*)1;
	args = *(t_thrarg*)ptr;
	if (args.thread.num + 1 < args.thread.total)
	{
		next = (t_thrarg){args.scene, args.img, args.index,
			(t_thrinfo){args.thread.num + 1, args.thread.total}, args.attr};
		if (pthread_create(&subthread, &(args.attr), &thread_start, &next))
			return (NULL);
		ret2 = cam_render_all(args.scene, args.img, args.thread);
		pthread_join(subthread, &ret);
		return ((void*)(long)(ret && (long)ret2));
	}
	return ((void*)(long)cam_render_all(args.scene, args.img, args.thread));
}

long	rerender_threaded(t_scene *sc, t_pixbuf **img, size_t index)
{
	pthread_t	subthread;
	t_thrarg	args;
	void		*ret;

	ret = (void*)1;
	if (sc->threads > 1)
	{
		args.scene = sc;
		args.img = img;
		args.index = index;
		args.thread = (t_thrinfo){0, sc->threads};
		if (pthread_attr_init(&(args.attr)))
			return (!put_error(ERR_THRINIT));
		pthread_attr_setdetachstate(&(args.attr), PTHREAD_CREATE_JOINABLE);
		if (!pthread_create(&subthread, &(args.attr), &rerender_start, &args))
			pthread_join(subthread, &ret);
		else
			put_error(ERR_THRINIT);
		pthread_attr_destroy(&(args.attr));
		return ((long)ret);
	}
	cam_render_one(*(t_camera*)(sc->cams.arr[index].obj),
		sc, (*img)[index], (t_thrinfo){0, 1});
	return (1);
}

void	*rerender_start(void *ptr)
{
	t_thrarg	args;
	t_thrarg	next;
	pthread_t	subthread;
	void		*ret;

	ret = (void*)1;
	args = *(t_thrarg*)ptr;
	if (args.thread.num + 1 < args.thread.total)
	{
		next = (t_thrarg){args.scene, args.img, args.index,
			(t_thrinfo){args.thread.num + 1, args.thread.total}, args.attr};
		if (pthread_create(&subthread, &(args.attr), &rerender_start, &next))
			return (NULL);
		cam_render_one(*(t_camera*)args.scene->cams.arr[args.index].obj,
			args.scene, (*args.img)[args.index], args.thread);
		pthread_join(subthread, &ret);
		return (ret);
	}
	cam_render_one(*(t_camera*)args.scene->cams.arr[args.index].obj,
		args.scene, (*args.img)[args.index], args.thread);
	return (ret);
}
