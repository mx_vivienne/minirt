/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   vector1_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/10 02:25:21 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/10 13:29:15 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_vect	v_add(t_vect u, t_vect v)
{
	return ((t_vect){
			u.x + v.x,
			u.y + v.y,
			u.z + v.z});
}

t_vect	v_subt(t_vect u, t_vect v)
{
	return ((t_vect){
			u.x - v.x,
			u.y - v.y,
			u.z - v.z});
}

t_vect	v_factor(t_vect u, double f)
{
	return ((t_vect){
			u.x * f,
			u.y * f,
			u.z * f});
}

t_vect	v_mult(t_vect u, t_vect v)
{
	return ((t_vect){
			u.x * v.x,
			u.y * v.y,
			u.z * v.z});
}

t_vect	v_div(t_vect u, double f)
{
	return ((t_vect){
			u.x / f,
			u.y / f,
			u.z / f});
}
