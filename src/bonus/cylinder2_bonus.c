/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   cylinder2_bonus.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/31 03:26:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/31 03:26:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_vect		cy_normal(t_vect p, t_obj o)
{
	t_cyl		cy;
	t_vect		normal;
	t_vect		cw;
	double		angle;

	cy = *(t_cyl*)o.obj;
	normal = v_unit(v_cross(cy.up, v_cross(v_subt(p, cy.center), cy.up)));
	if (o.mat.surface.type != M_SURF_REGULAR)
	{
		cw = v_unit(v_cross(cy.up, normal));
		if (o.mat.surface.type == M_SURF_SINE)
		{
			angle = sin(8 * M_PI * (1 - v_scalar(cw, cy.right)));
			return (v_rotate(normal, cw, .2 * angle));
		}
	}
	return (normal);
}

t_coords	cy_uv(t_vect p, t_obj o)
{
	t_coords	uv;
	t_cyl		cy;

	cy = *(t_cyl*)o.obj;
	p = m_apply(cy.mtx, v_subt(p, cy.center));
	uv.x = .5 + (atan2(p.x, p.z)) / (2 * M_PI);
	uv.y = 1 - ((p.y + 1) / 2);
	return (uv);
}
