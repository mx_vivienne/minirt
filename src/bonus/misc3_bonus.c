/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   misc3_bonus.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/09 20:07:59 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/09 20:08:00 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static int	next_line(int fd, char ***words, int lineno)
{
	char	*line;
	int		res;

	res = get_next_line(fd, &line);
	if (res == 0)
	{
		free(line);
		return (0);
	}
	if (res < 0)
		return (!put_error_line(ERR_IMG_READ, lineno) - 1);
	*words = ft_split(line, ' ');
	free(line);
	if (words == NULL)
		return (!put_error(ERR_ALLOC));
	return (1);
}

int			next_word(int fd, char **tok, int lineno, int do_reset)
{
	static char		**words = NULL;
	static size_t	pos = 0;
	int				res;

	if (do_reset)
	{
		pos = 0;
		words_destroy(&words);
		return (0);
	}
	while (words == NULL || words[pos] == NULL || words[pos][0] == '#')
	{
		pos = 0;
		words_destroy(&words);
		res = next_line(fd, &words, lineno);
		if (res != 1)
			return (res);
	}
	pos++;
	*tok = words[pos - 1];
	return (1);
}

int			get_word(int fd, char **word, int lineno)
{
	int	res;

	res = next_word(fd, word, lineno, 0);
	if (res > 0)
		return (1);
	if (res == 0)
		return (!put_error_line(ERR_IMG_EOF, lineno));
	return (0);
}

int			read_size(size_t *i, char *str, size_t max, int lineno)
{
	size_t	offset;

	offset = 0;
	*i = 0;
	if (*str == '-')
		return (!put_error_line(ERR_IMG_READ, lineno));
	if (*str == '+')
		offset++;
	if (str[offset] == '\0')
		return (!put_error_line(ERR_IMG_READ, lineno));
	while (str[offset] != '\0')
	{
		if (!ft_isdigit(str[offset]))
			return (!put_error_line(ERR_IMG_READ, lineno));
		*i = *i * 10 + (str[offset] - '0');
		offset++;
	}
	if (max != 0 && *i > max)
		return (!put_error_line(ERR_IMG_READ, lineno));
	return (1);
}
