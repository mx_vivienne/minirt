/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   matrix1_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/27 00:08:51 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/27 00:08:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_matrix	m_add(t_matrix m, t_matrix n)
{
	return ((t_matrix){{
		v_add(m.col[0], n.col[0]),
		v_add(m.col[1], n.col[1]),
		v_add(m.col[2], n.col[2])}});
}

t_matrix	m_subt(t_matrix m, t_matrix n)
{
	return ((t_matrix){{
		v_subt(m.col[0], n.col[0]),
		v_subt(m.col[1], n.col[1]),
		v_subt(m.col[2], n.col[2])}});
}

t_matrix	m_factor(t_matrix m, double f)
{
	return ((t_matrix){{
		v_factor(m.col[0], f),
		v_factor(m.col[1], f),
		v_factor(m.col[2], f)}});
}

t_matrix	m_mult(t_matrix m, t_matrix n)
{
	return ((t_matrix){{
		(t_vect){
	m.col[0].x * n.col[0].x + m.col[1].x * n.col[0].y + m.col[2].x * n.col[0].z,
	m.col[0].y * n.col[0].x + m.col[1].y * n.col[0].y + m.col[2].y * n.col[0].z,
	m.col[0].z * n.col[0].x + m.col[1].z * n.col[0].y + m.col[2].z * n.col[0].z
		}, (t_vect){
	m.col[0].x * n.col[1].x + m.col[1].x * n.col[1].y + m.col[2].x * n.col[1].z,
	m.col[0].y * n.col[1].x + m.col[1].y * n.col[1].y + m.col[2].y * n.col[1].z,
	m.col[0].z * n.col[1].x + m.col[1].z * n.col[1].y + m.col[2].z * n.col[1].z
		}, (t_vect){
	m.col[0].x * n.col[2].x + m.col[1].x * n.col[2].y + m.col[2].x * n.col[2].z,
	m.col[0].y * n.col[2].x + m.col[1].y * n.col[2].y + m.col[2].y * n.col[2].z,
	m.col[0].z * n.col[2].x + m.col[1].z * n.col[2].y + m.col[2].z * n.col[2].z}
		}});
}

t_vect		m_apply(t_matrix m, t_vect v)
{
	return ((t_vect){
		v.x * m.col[0].x + v.y * m.col[1].x + v.z * m.col[2].x,
		v.x * m.col[0].y + v.y * m.col[1].y + v.z * m.col[2].y,
		v.x * m.col[0].z + v.y * m.col[1].z + v.z * m.col[2].z});
}
