/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   input4_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/17 00:32:48 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/17 00:32:53 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int			th_read(char **words, t_scene *sc, int lineno)
{
	return (ensure_arguments(words, 2, lineno) &&
		read_uint((unsigned*)&(sc->threads), words[1], lineno, INTMODE_NOZERO));
}

int			ensure_arguments(char **args, int required, int lineno)
{
	int	count;

	if (args == NULL)
		return (!put_error_line(ERR_LINEREAD, lineno));
	count = 0;
	while (count < required)
	{
		if (args[count] == NULL)
			return (!put_error_line(ERR_FEWARGS, lineno));
		count++;
	}
	if (args[count] != NULL)
		return (!put_error_line(ERR_MANYARGS, lineno));
	return (1);
}

int			ensure_args_range(char **args, int min, int max, int lineno)
{
	int	count;

	if (args == NULL)
		return (!put_error_line(ERR_LINEREAD, lineno));
	count = 0;
	while (count < max)
	{
		if (args[count] == NULL)
		{
			if (count < min)
				return (!put_error_line(ERR_FEWARGS, lineno));
			return (1);
		}
		count++;
	}
	if (args[count] != NULL)
		return (!put_error_line(ERR_MANYARGS, lineno));
	return (1);
}
