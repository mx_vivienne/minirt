/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   vector4_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/31 00:49:20 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/31 00:49:22 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

/*
** https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
*/

t_vect	v_rotate(t_vect v, t_vect axis, double angle)
{
	return (v_add3(
		v_factor(v, cos(angle)),
		v_factor(v_cross(axis, v), sin(angle)),
		v_factor(axis, v_scalar(axis, v) * (1 - cos(angle)))));
}
