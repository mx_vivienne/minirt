/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   matrix2_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/27 00:57:48 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/27 00:57:49 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_matrix	rotation_matrix(t_vect from, t_vect to)
{
	t_vect	a;
	double	c;
	double	m;
	double	s;

	c = v_scalar(to, from);
	if (fabs(c) > 1 - EPSILON)
	{
		return ((t_matrix){
			(t_vect){1, 0, 0}, (t_vect){0, 1, 0}, (t_vect){0, 0, 1}});
	}
	a = v_unit(v_cross(from, to));
	m = 1 - c;
	s = sin(acos(c));
	return ((t_matrix){(t_vect){
			a.x * a.x * m + c,
			a.x * a.y * m + s * a.z,
			a.x * a.z * m - s * a.y}, (t_vect){
			a.y * a.x * m - s * a.z,
			a.y * a.y * m + c,
			a.y * a.z * m + s * a.x}, (t_vect){
			a.z * a.x * m + s * a.y,
			a.z * a.y * m - s * a.x,
			a.z * a.z * m + c}});
}

t_matrix	stretch_matrix(t_vect factors)
{
	return ((t_matrix){
		(t_vect){factors.x, 0, 0},
		(t_vect){0, factors.y, 0},
		(t_vect){0, 0, factors.z}});
}
