/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   light2_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/28 05:03:09 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/28 05:03:10 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int		l2_put(t_light2 l, t_color c, t_scene *sc)
{
	t_light2	*lt;
	t_obj		o;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	lt = malloc(sizeof(t_light2));
	if (lt == NULL)
		return (!put_error(ERR_ALLOC));
	l.dir = v_factor(l.dir, -1);
	*lt = l;
	o.type = OBJ_LIGHT2;
	o.mat = (t_material){NULL, c, {0}, {0}};
	o.obj = (void*)lt;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

int		l2_read(char **words, t_scene *sc, int lineno)
{
	t_light2	l;
	t_color		c;
	double		intensity;

	if (!ensure_arguments(words, 4, lineno) ||
		!v_read(&(l.dir), words[1], lineno, VECMODE_NORM) ||
		!read_double(&intensity, words[2], lineno,
					DBLMODE_POS | DBLMODE_UNIT) ||
		!c_read(&c, words[3], lineno) ||
		!l2_put(l, c_shade(c, intensity), sc))
		return (0);
	return (1);
}
