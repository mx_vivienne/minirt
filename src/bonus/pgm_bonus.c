/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   pgm_bonus.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/21 00:24:04 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/21 00:24:06 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static int	read_pgm_pixels(t_bumpmap *img, size_t max, int fd, int ln)
{
	char	*word;
	size_t	offset;
	size_t	temp;

	img->data = malloc(img->w * img->h * sizeof(double));
	if (img->data == NULL)
		return (next_word(0, NULL, 0, 1) + !put_error(ERR_ALLOC));
	offset = 0;
	while (offset < img->w * img->h)
	{
		if (get_word(fd, &word, ln) && read_size(&temp, word, max, ln))
			img->data[offset] = (double)temp / max;
		else
		{
			free(img->data);
			return (next_word(0, NULL, 0, 1));
		}
		offset++;
	}
	return (1);
}

int			read_pgm(t_bumpmap *img, int fd, int ln)
{
	char	*word;
	size_t	max;

	if (!get_word(fd, &word, ln))
		return (0);
	if (0 != ft_strncmp(word, "P2", 3))
		return (next_word(0, NULL, 0, 1) + !put_error_line(ERR_IMG_READ, ln));
	if (!get_word(fd, &word, ln) || !read_size(&(img->w), word, 0, ln) ||
		!get_word(fd, &word, ln) || !read_size(&(img->h), word, 0, ln) ||
		!get_word(fd, &word, ln) || !read_size(&max, word, 0, ln) ||
		!read_pgm_pixels(img, max, fd, ln))
		return (next_word(0, NULL, 0, 1));
	if (next_word(fd, &word, ln, 0))
		return (next_word(0, NULL, 0, 1) + !put_error_line(ERR_IMG_LONG, ln));
	return (1);
}
