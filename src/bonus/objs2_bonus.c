/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   objs2_bonus.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/26 21:18:45 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/26 21:18:46 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static unsigned		obj_color_checker(t_coords uv, t_obj o)
{
	if (o.type == OBJ_SPHERE || o.type == OBJ_CYLINDER || o.type == OBJ_CONE)
	{
		uv.x *= 6;
		uv.y *= 6;
	}
	return ((unsigned)fabs(fmod(
		floor(uv.x / o.mat.color.scale) +
		floor(uv.y / o.mat.color.scale), 2)));
}

static t_color		obj_color_img(t_coords uv, t_obj o, t_pixbuf img)
{
	size_t	offset;

	if (o.type == OBJ_PLANE)
	{
		uv.x = fmod(uv.x, 1);
		uv.x += uv.x < 0;
		uv.y = fmod(uv.y, 1);
		uv.y += uv.y < 0;
	}
	offset = img.w * (size_t)(uv.y * img.h) + (size_t)(uv.x * img.w);
	return (img.data[offset]);
}

t_color				obj_color(t_hit h)
{
	t_coords	uv;
	t_color		*cols;

	if (h.obj.mat.name == NULL)
		return (h.obj.mat.fallback);
	cols = (t_color*)h.obj.mat.color.data;
	if (h.obj.mat.color.type == M_COL_FLAT)
		return (cols[0]);
	if (h.obj.mat.color.type == M_COL_RAINBOW)
		return (rainbow_color(h.normal, h.obj, cols[0]));
	if (h.obj.mat.color.type == M_COL_RGB)
		return (c_from_vect(h.normal));
	uv = obj_uv(h.point, h.obj);
	if (h.obj.mat.color.type == M_COL_CHECKER)
		return (((t_color*)h.obj.mat.color.data)[obj_color_checker(uv, h.obj)]);
	if (h.obj.mat.color.type == M_COL_IMAGE)
		return (obj_color_img(uv, h.obj, *(t_pixbuf*)h.obj.mat.color.data));
	return (h.obj.mat.fallback);
}
