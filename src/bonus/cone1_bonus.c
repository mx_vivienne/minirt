/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   cone1_bonus.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/27 21:39:45 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/27 21:39:46 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static t_matrix	co_affine_matrix(t_cone co)
{
	t_matrix	mtx;

	mtx = rotation_matrix(co.up, (t_vect){0, 1, 0});
	mtx = m_mult(
		stretch_matrix((t_vect){1 / co.radius, 1 / co.size, 1 / co.radius}),
		mtx);
	return (mtx);
}

int				co_put(t_cone co, t_material mat, t_scene *sc, int capped)
{
	t_cone	*cone;

	if (capped)
	{
		if (!ci_put((t_circle){v_add(co.center, v_factor(co.up, co.size / 2)),
				co.up, {0, 0, 0}, {0, 0, 0}, co.radius}, mat, sc) ||
			!ci_put((t_circle){v_add(co.center, v_factor(co.up, -.5 * co.size)),
				v_factor(co.up, -1), {0, 0, 0}, {0, 0, 0}, co.radius}, mat, sc))
			return (0);
	}
	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	cone = malloc(sizeof(t_cone));
	if (cone == NULL)
		return (!put_error(ERR_ALLOC));
	co.radius /= 2;
	co.size /= 2;
	co.right = (t_vect){0, 0, 1};
	if (fabs(v_scalar(co.right, co.up)) > 1.0 - EPSILON)
		co.right = (t_vect){1, 0, 0};
	*cone = (t_cone){co.center, co.up, v_unit(v_cross(co.right, co.up)),
		co.radius, co.size, co_affine_matrix(co)};
	sc->objs.arr[sc->objs.len] = (t_obj){OBJ_CONE, mat, (void*)cone};
	sc->objs.len += 1;
	return (1);
}

double			co_ray(t_ray r, t_cone co)
{
	t_vect	f;
	double	disc;

	r = (t_ray){m_apply(co.mtx, v_subt(r.origin, co.center)),
		m_apply(co.mtx, r.dir)};
	f.x = r.dir.x * r.dir.x - r.dir.y * r.dir.y + r.dir.z * r.dir.z;
	f.y = 2 * (r.dir.x * r.origin.x - r.dir.y * r.origin.y +
		r.dir.z * r.origin.z);
	f.z = r.origin.x * r.origin.x - r.origin.y * r.origin.y +
		r.origin.z * r.origin.z;
	disc = f.y * f.y - 4.0 * f.x * f.z;
	if (disc < 0)
		return (-1.0);
	disc = sqrt(disc);
	f.x *= 2;
	return (test_hits(r, (-f.y + disc) / f.x, (-f.y - disc) / f.x));
}

int				co_read(char **words, t_scene *sc, int lineno)
{
	t_cone		co;
	t_material	mat;
	int			capped;

	capped = 0;
	if (ensure_args_range(words, 6, 7, lineno) &&
		v_read(&(co.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(co.up), words[2], lineno, VECMODE_NORM) &&
		read_double(&(co.radius), words[3], lineno,
			DBLMODE_NOZERO | DBLMODE_POS) &&
		read_double(&(co.size), words[4], lineno,
			DBLMODE_NOZERO | DBLMODE_POS) &&
		m_name_read(&mat, words[5], *sc, lineno))
	{
		if (words[6] != NULL)
		{
			if (0 != ft_strncmp(words[6], "capped", 7))
				return (!put_error_line(ERR_KEYWORD, lineno));
			capped = 1;
		}
		return (co_put(co, mat, sc, capped));
	}
	return (0);
}
