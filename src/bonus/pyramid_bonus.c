/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   pyramid_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/15 23:36:25 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/15 23:36:36 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int				py_put(t_pyram py, t_material mat, t_scene *sc)
{
	t_vect		n;
	t_square	s;

	n = (t_vect){0, 0, 0};
	s = (t_square){(t_vect){0, 0, 0}, (t_vect){0, 0, 0}, (t_vect){0, 0, 0},
		(t_vect){0, 0, 0}, 0};
	if (sq_put((t_square){py.center, v_factor(py.up, -1),
			py.front, {0, 0, 0}, py.width}, mat, sc, 0) &&
		tr_put((t_tri){py.pts[1], py.pts[2], py.pts[0], n, n, n, s}, mat, sc) &&
		tr_put((t_tri){py.pts[2], py.pts[3], py.pts[0], n, n, n, s}, mat, sc) &&
		tr_put((t_tri){py.pts[3], py.pts[4], py.pts[0], n, n, n, s}, mat, sc) &&
		tr_put((t_tri){py.pts[4], py.pts[1], py.pts[0], n, n, n, s}, mat, sc))
		return (1);
	return (0);
}

int				py_calc_points(t_pyram *py, int lineno)
{
	double	half;

	if (!v_are_orthogonal(py->up, py->right))
		return (!put_error_line(ERR_VNOTORTH, lineno));
	half = py->width / 2;
	py->front = v_cross(py->up, py->right);
	py->pts[0] = v_add(py->center, v_factor(py->up, py->height));
	py->pts[1] = v_add3(py->center, v_factor(py->right, half),
				v_factor(py->front, half));
	py->pts[2] = v_add3(py->center, v_factor(py->right, half),
				v_factor(py->front, -half));
	py->pts[3] = v_add3(py->center, v_factor(py->right, -half),
				v_factor(py->front, -half));
	py->pts[4] = v_add3(py->center, v_factor(py->right, -half),
				v_factor(py->front, half));
	return (1);
}

int				py_read(char **words, t_scene *sc, int lineno)
{
	t_pyram		py;
	t_material	mat;

	if (ensure_arguments(words, 7, lineno) &&
		v_read(&(py.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(py.up), words[2], lineno, VECMODE_NORM) &&
		v_read(&(py.right), words[3], lineno, VECMODE_NORM) &&
		read_double(&(py.width), words[4], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		read_double(&(py.height), words[5], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		py_calc_points(&py, lineno) &&
		m_name_read(&mat, words[6], *sc, lineno) &&
		py_put(py, mat, sc))
		return (1);
	return (0);
}
