/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   vector3_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/19 17:59:40 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/19 17:59:43 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_vect	v_add3(t_vect u, t_vect v, t_vect w)
{
	return ((t_vect){
			u.x + v.x + w.x,
			u.y + v.y + w.y,
			u.z + v.z + w.z});
}

t_vect	v_add4(t_vect u, t_vect v, t_vect w, t_vect x)
{
	return ((t_vect){
			u.x + v.x + w.x + x.x,
			u.y + v.y + w.y + x.y,
			u.z + v.z + w.z + x.z});
}

double	v_dist(t_vect u, t_vect v)
{
	return (sqrt(
		(u.x - v.x) * (u.x - v.x) +
		(u.y - v.y) * (u.y - v.y) +
		(u.z - v.z) * (u.z - v.z)));
}

int		v_are_orthogonal(t_vect u, t_vect v)
{
	return (fabs(v_scalar(u, v)) < EPSILON);
}

int		v_exact_equals(t_vect u, t_vect v)
{
	return (u.x == v.x && u.y == v.y && u.z == v.z);
}
