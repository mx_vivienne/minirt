/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   cone2_bonus.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/31 03:35:39 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/31 03:35:40 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_vect		co_normal(t_vect p, t_obj o)
{
	t_cone		co;
	t_vect		normal;
	t_vect		cw;
	double		angle;

	co = *(t_cone*)o.obj;
	p = v_subt(p, co.center);
	normal = v_unit(v_cross(v_cross(p, co.up), p));
	if (o.mat.surface.type != M_SURF_REGULAR)
	{
		cw = v_unit(v_cross(co.up, normal));
		if (o.mat.surface.type == M_SURF_SINE)
		{
			angle = sin(8 * M_PI * (1 - v_scalar(cw, co.right)));
			return (v_rotate(normal, cw, .2 * angle));
		}
	}
	return (normal);
}

t_coords	co_uv(t_vect p, t_obj o)
{
	t_coords	uv;
	t_cone		co;

	co = *(t_cone*)o.obj;
	p = m_apply(co.mtx, v_subt(p, co.center));
	uv.x = .5 + (atan2(p.x, p.z)) / (2 * M_PI);
	uv.y = 1 - ((p.y + 1) / 2);
	return (uv);
}
