/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   light1_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/13 20:18:14 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/13 20:18:24 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int		l_put(t_light l, t_color c, t_scene *sc)
{
	t_obj	o;
	t_light	*lt;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	lt = malloc(sizeof(t_light));
	if (lt == NULL)
		return (!put_error(ERR_ALLOC));
	*lt = l;
	o.type = OBJ_LIGHT;
	o.mat = (t_material){NULL, c, {0}, {0}};
	o.obj = (void*)lt;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

int		l_read(char **words, t_scene *sc, int lineno)
{
	t_light		l;
	t_color		c;
	double		intensity;

	if (!ensure_arguments(words, 4, lineno) ||
		!v_read(&(l.pos), words[1], lineno, VECMODE_ANY) ||
		!read_double(&intensity, words[2], lineno,
					DBLMODE_POS | DBLMODE_UNIT) ||
		!c_read(&c, words[3], lineno) ||
		!l_put(l, c_shade(c, intensity), sc))
		return (0);
	return (1);
}

t_color	l_trace(t_scene sc, t_hit hit, t_color color, t_obj light)
{
	t_ray	shadow;
	t_hit	block;
	double	angle;

	if (light.type == OBJ_LIGHT)
		shadow = r_through(hit.point, ((t_light*)light.obj)->pos);
	else
		shadow = (t_ray){hit.point, ((t_light2*)light.obj)->dir};
	shadow.origin = v_add(shadow.origin, v_factor(shadow.dir, EPSILON));
	angle = v_scalar(v_unit(hit.normal), v_unit(shadow.dir));
	if (angle <= 0.0)
		return ((t_color){0, 0, 0});
	block = r_trace(sc.objs, shadow, 0, 1);
	if (block.dist > 0 && block.obj.type != OBJ_SKYBOX &&
		block.dist < v_len(shadow.dir))
	{
		return ((t_color){0, 0, 0});
	}
	return (c_shade(c_mult(color, light.mat.fallback), angle));
}

t_color	lighting(t_scene sc, t_hit hit)
{
	t_color		sum;
	size_t		offset;
	t_color		color;
	unsigned	type;

	offset = 0;
	color = obj_color(hit);
	if (hit.obj.type == OBJ_SKYBOX)
		return (color);
	sum = c_mult(color, sc.amb);
	while (offset < sc.objs.len)
	{
		type = sc.objs.arr[offset].type;
		if (type == OBJ_LIGHT || type == OBJ_LIGHT2)
			sum = c_add(sum, l_trace(sc, hit, color, sc.objs.arr[offset]));
		offset++;
	}
	return (sum);
}
