/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   material_bonus.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/20 02:33:13 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/20 02:33:14 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static int	mat_check_name(t_material mat, t_scene *sc, int lineno)
{
	size_t	offset;

	if (mat.name == NULL)
		return (1);
	if (ft_strlen(mat.name) > M_MAX_NAMELEN - 1)
		return (!put_error_line(ERR_MATLONG, lineno));
	offset = 0;
	while (mat.name[offset] != '\0')
	{
		if (!ft_isalnum(mat.name[offset]))
			return (!put_error_line(ERR_MATCHAR, lineno));
		offset++;
	}
	offset = 0;
	while (offset < sc->mats.len)
	{
		if (!ft_strncmp(((t_material*)(sc->mats.arr[offset].obj))->name,
			mat.name, M_MAX_NAMELEN))
			return (!put_error_line(ERR_MATDUP, lineno));
		offset++;
	}
	return (1);
}

int			mat_put(t_material mat, t_scene *sc, int lineno, t_material *ptr)
{
	t_obj		o;

	if (!ensure_arr_size(&(sc->mats)) || !mat_check_name(mat, sc, lineno))
		return (0);
	ptr = malloc(sizeof(t_material));
	if (ptr == NULL)
		return (!put_error(ERR_ALLOC));
	*ptr = mat;
	o.type = OBJ_MATERIAL;
	o.obj = (void*)ptr;
	sc->mats.arr[sc->mats.len] = o;
	sc->mats.len += 1;
	return (1);
}

int			mat_read(char **words, t_scene *sc, int lineno)
{
	t_material	mat;

	mat = (t_material){NULL, {0}, {0}, {0}};
	if (ensure_arguments(words, 4, lineno) &&
		mat_read_color(&(mat.color), words[2], lineno) &&
		mat_read_surface(&(mat.surface), words[3], lineno))
	{
		mat.name = ft_strdup(words[1]);
		if (mat.name != NULL && mat_put(mat, sc, lineno, NULL))
			return (1);
	}
	mat_destroy(&mat);
	return (0);
}

void		mat_destroy(t_material *mat)
{
	if (mat == NULL)
		return ;
	free(mat->name);
	mat->name = NULL;
	if (mat->color.type == M_COL_IMAGE && mat->color.data != NULL)
		free(((t_pixbuf*)(mat->color.data))->data);
	free(mat->color.data);
	mat->color.data = NULL;
	free(mat->surface.data);
	mat->surface.data = NULL;
}

int			m_name_read(t_material *m, char *param, t_scene sc, int ln)
{
	size_t	offset;

	*m = (t_material){NULL, {0}, {0}, {0}};
	if (ft_strchr(param, ',') != NULL)
		return (c_read(&(m->fallback), param, ln));
	if (ft_strlen(param) > M_MAX_NAMELEN - 1)
		return (!put_error_line(ERR_MATLONG, ln));
	if (!test_string(param, *ft_isalnum))
		return (!put_error_line(ERR_MATCHAR, ln));
	offset = 0;
	while (offset < sc.mats.len)
	{
		if (!ft_strncmp(param, ((t_material*)(sc.mats.arr[offset].obj))->name,
			M_MAX_NAMELEN))
		{
			*m = *(t_material*)sc.mats.arr[offset].obj;
			return (1);
		}
		offset++;
	}
	return (!put_error_line(ERR_MATNEW, ln));
}
