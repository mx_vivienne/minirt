/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   rainbow_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/26 02:18:19 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/26 02:18:21 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_color			rainbow_color(t_vect normal, t_obj obj, t_color base)
{
	t_vect	axis;

	axis = (t_vect){0, 1, 0};
	if (obj.type == OBJ_CYLINDER)
		axis = ((t_cyl*)obj.obj)->right;
	if (obj.type == OBJ_CONE)
		axis = ((t_cone*)obj.obj)->right;
	return (filter_hue_rotate(base, (1 + v_scalar(normal, axis)) * 180));
}
