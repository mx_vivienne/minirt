/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   defines_bonus.h                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/13 19:04:26 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/13 19:04:28 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINES_BONUS_H
# define DEFINES_BONUS_H

# ifndef NAME
#  define NAME "miniRT"
# endif

# ifndef COMPILE_FOR_MAC
#  define COMPILE_FOR_MAC 0
# endif

# define EPSILON		0.0000001f
# define OBJS_BUFFER	32

# define OBJ_MATERIAL	7627117
# define OBJ_SKYBOX		7957363
# define OBJ_SPHERE		28787
# define OBJ_CYLINDER	31075
# define OBJ_CONE		28515
# define OBJ_CUBE		25187
# define OBJ_PYRAMID	31088
# define OBJ_PLANE		27760
# define OBJ_SQUARE		29043
# define OBJ_TRIANGLE	29300
# define OBJ_CIRCLE		26979
# define OBJ_ANTIALIAS	16705
# define OBJ_THREADS	18516
# define OBJ_LIGHT2		12908
# define OBJ_LIGHT		108
# define OBJ_CAMERA		99
# define OBJ_RESOLUTION	82
# define OBJ_AMBIENT	65
# define OBJ_NONE		0

# define FILTER_NONE	0
# define FILTER_SEPIA	1
# define FILTER_GREYSCL	2
# define FILTER_INVERT	3
# define FILTER_ROT_HUE	4
# define FILTER_SCNLINE	5

# define INTMODE_ANY	0
# define INTMODE_POS	1
# define INTMODE_NOZERO	2
# define INTMODE_ANGLE	4
# define INTMODE_BYTE	8

# define DBLMODE_ANY	0
# define DBLMODE_POS	1
# define DBLMODE_NOZERO	2
# define DBLMODE_UNIT	4

# define VECMODE_ANY	0
# define VECMODE_NORM	16

# define M_MAX_NAMELEN	16

# define M_COL_FLAT		0
# define M_COL_CHECKER	1
# define M_COL_RAINBOW	2
# define M_COL_RGB		3
# define M_COL_IMAGE	4

# define M_SURF_REGULAR	0
# define M_SURF_SINE	1

#endif
