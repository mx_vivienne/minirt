/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   typedefs_bonus.h                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/12 19:52:43 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/12 19:52:44 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef TYPEDEFS_BONUS_H
# define TYPEDEFS_BONUS_H

# include <stddef.h>
# include <pthread.h>
# include <unistd.h>

typedef struct		s_color
{
	unsigned		r;
	unsigned		g;
	unsigned		b;
}					t_color;

typedef struct		s_property
{
	unsigned		type;
	double			scale;
	void			*data;

}					t_property;

typedef struct		s_material
{
	char			*name;
	t_color			fallback;
	t_property		color;
	t_property		surface;
}					t_material;

typedef struct		s_pixbuf
{
	size_t			w;
	size_t			h;
	t_color			*data;
}					t_pixbuf;

typedef struct		s_bumpmap
{
	size_t			w;
	size_t			h;
	double			*data;
}					t_bumpmap;

typedef struct		s_vect
{
	double			x;
	double			y;
	double			z;
}					t_vect;

typedef struct		s_coords
{
	double			x;
	double			y;
}					t_coords;

typedef struct		s_matrix
{
	t_vect			col[3];
}					t_matrix;

typedef struct		s_obj
{
	unsigned int	type;
	t_material		mat;
	void			*obj;
}					t_obj;

typedef struct		s_light
{
	t_vect			pos;
}					t_light;

typedef struct		s_light2
{
	t_vect			dir;
}					t_light2;

typedef struct		s_ray
{
	t_vect			origin;
	t_vect			dir;
}					t_ray;

typedef struct		s_hit
{
	t_obj			obj;
	t_vect			point;
	t_vect			normal;
	double			dist;
}					t_hit;

typedef struct		s_sphere
{
	t_vect			center;
	double			radius;
}					t_sphere;

/*
** note: "size" is half the cylinder/cone height here,
** similar to how we use radius instead of diameter
*/
typedef struct		s_cyl
{
	t_vect			center;
	t_vect			up;
	t_vect			right;
	double			radius;
	double			size;
	t_matrix		mtx;
}					t_cyl;

typedef struct		s_cone
{
	t_vect			center;
	t_vect			up;
	t_vect			right;
	double			radius;
	double			size;
	t_matrix		mtx;
}					t_cone;

typedef struct		s_cube
{
	t_vect			center;
	t_vect			up;
	t_vect			down;
	t_vect			right;
	t_vect			left;
	t_vect			front;
	t_vect			back;
	double			size;
}					t_cube;

typedef struct		s_pyram
{
	t_vect			center;
	t_vect			up;
	t_vect			right;
	t_vect			front;
	t_vect			pts[5];
	double			width;
	double			height;
}					t_pyram;

typedef struct		s_plane
{
	t_vect			origin;
	t_vect			normal;
	t_vect			up;
	t_vect			right;
}					t_plane;

typedef struct		s_square
{
	t_vect			center;
	t_vect			normal;
	t_vect			up;
	t_vect			right;
	double			size;
}					t_square;

typedef struct		s_circle
{
	t_vect			center;
	t_vect			normal;
	t_vect			up;
	t_vect			right;
	double			radius;
}					t_circle;

typedef struct		s_tri
{
	t_vect			a;
	t_vect			b;
	t_vect			c;
	t_vect			up;
	t_vect			right;
	t_vect			normal;
	t_square		bounds;
}					t_tri;

typedef struct		s_filter
{
	int				type;
	double			*args;
}					t_filter;

typedef struct		s_camera
{
	t_ray			r;
	t_vect			vup;
	t_vect			right;
	unsigned int	fov;
	double			anaglyph;
	t_filter		f;
}					t_camera;

typedef struct		s_campixel
{
	size_t			x;
	size_t			y;
	t_vect			corner;
	t_vect			down;
	t_vect			right;
}					t_campixel;

typedef struct		s_arr
{
	size_t			len;
	size_t			mem;
	t_obj			*arr;
}					t_arr;

typedef struct		s_scene
{
	size_t			x;
	size_t			y;
	size_t			aa;
	size_t			threads;
	t_color			amb;
	t_arr			objs;
	t_arr			cams;
	t_arr			mats;
}					t_scene;

typedef struct		s_mlx_img
{
	void			*img;
	char			*ptr;
	int				bpp;
	int				linelen;
	int				endian;
}					t_mlx_img;

typedef struct		s_mlx_ptrs
{
	void			*mlx;
	void			*win;
	t_mlx_img		img;
}					t_mlx_ptrs;

typedef struct		s_mlx_data
{
	t_mlx_ptrs		ptrs;
	t_pixbuf		*img;
	t_scene			scene;
	size_t			current;
	size_t			len;
}					t_mlx_data;

typedef struct		s_thrinfo
{
	size_t			num;
	size_t			total;
}					t_thrinfo;

typedef struct		s_thrarg
{
	t_scene			*scene;
	t_pixbuf		**img;
	size_t			index;
	t_thrinfo		thread;
	pthread_attr_t	attr;
}					t_thrarg;

#endif
