/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   cube_bonus.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/15 20:46:13 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/15 20:46:15 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int				cb_put(t_cube cb, t_material mat, t_scene *sc)
{
	double	half;

	half = cb.size / 2;
	if (sq_put((t_square){v_add(cb.center, v_factor(cb.up, half)),
			cb.up, cb.back, {0, 0, 0}, cb.size}, mat, sc, 0) &&
		sq_put((t_square){v_add(cb.center, v_factor(cb.down, half)),
			cb.down, cb.back, {0, 0, 0}, cb.size}, mat, sc, 0) &&
		sq_put((t_square){v_add(cb.center, v_factor(cb.right, half)),
			cb.right, cb.up, {0, 0, 0}, cb.size}, mat, sc, 0) &&
		sq_put((t_square){v_add(cb.center, v_factor(cb.left, half)),
			cb.left, cb.up, {0, 0, 0}, cb.size}, mat, sc, 0) &&
		sq_put((t_square){v_add(cb.center, v_factor(cb.back, half)),
			cb.back, cb.up, {0, 0, 0}, cb.size}, mat, sc, 0) &&
		sq_put((t_square){v_add(cb.center, v_factor(cb.front, half)),
			cb.front, cb.up, {0, 0, 0}, cb.size}, mat, sc, 0))
		return (1);
	return (0);
}

int				cb_calc_normals(t_cube *cb, int lineno)
{
	if (!v_are_orthogonal(cb->up, cb->right))
		return (!put_error_line(ERR_VNOTORTH, lineno));
	cb->back = v_cross(cb->up, cb->right);
	cb->down = v_factor(cb->up, -1);
	cb->left = v_factor(cb->right, -1);
	cb->front = v_factor(cb->back, -1);
	return (1);
}

int				cb_read(char **words, t_scene *sc, int lineno)
{
	t_cube		cb;
	t_material	mat;

	if (ensure_arguments(words, 6, lineno) &&
		v_read(&(cb.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(cb.up), words[2], lineno, VECMODE_NORM) &&
		v_read(&(cb.right), words[3], lineno, VECMODE_NORM) &&
		cb_calc_normals(&cb, lineno) &&
		read_double(&(cb.size), words[4], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		m_name_read(&mat, words[5], *sc, lineno) &&
		cb_put(cb, mat, sc))
		return (1);
	return (0);
}
