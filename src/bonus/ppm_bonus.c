/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ppm_bonus.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/20 19:27:38 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/20 19:27:39 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static int	read_ppm_triplet(t_pixbuf *img, size_t offset, size_t max, int i[])
{
	t_color	color;
	size_t	temp;
	char	*word;

	if (get_word(i[0], &word, i[1]) && read_size(&temp, word, max, i[1]))
		color.r = (unsigned)((double)temp / max * 255);
	else
		return (0);
	if (get_word(i[0], &word, i[1]) && read_size(&temp, word, max, i[1]))
		color.g = (unsigned)((double)temp / max * 255);
	else
		return (0);
	if (get_word(i[0], &word, i[1]) && read_size(&temp, word, max, i[1]))
		color.b = (unsigned)((double)temp / max * 255);
	else
		return (0);
	img->data[offset] = color;
	return (1);
}

static int	read_ppm_pixels(t_pixbuf *img, size_t max, int fd, int ln)
{
	size_t	offset;

	img->data = malloc(img->w * img->h * sizeof(t_color));
	if (img->data == NULL)
		return (next_word(0, NULL, 0, 1) + !put_error(ERR_ALLOC));
	offset = 0;
	while (offset < img->w * img->h)
	{
		if (!read_ppm_triplet(img, offset, max, (int[]){fd, ln}))
		{
			free(img->data);
			return (next_word(0, NULL, 0, 1));
		}
		offset++;
	}
	return (1);
}

int			read_ppm(t_pixbuf *img, int fd, int ln)
{
	char	*word;
	size_t	max;

	if (!get_word(fd, &word, ln))
		return (0);
	if (0 != ft_strncmp(word, "P3", 3))
		return (next_word(0, NULL, 0, 1) + !put_error_line(ERR_IMG_READ, ln));
	if (!get_word(fd, &word, ln) || !read_size(&(img->w), word, 0, ln) ||
		!get_word(fd, &word, ln) || !read_size(&(img->h), word, 0, ln) ||
		!get_word(fd, &word, ln) || !read_size(&max, word, 0, ln) ||
		!read_ppm_pixels(img, max, fd, ln))
		return (next_word(0, NULL, 0, 1));
	if (next_word(fd, &word, ln, 0))
		return (next_word(0, NULL, 0, 1) + !put_error_line(ERR_IMG_LONG, ln));
	return (1);
}
