/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   camera3_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/28 03:04:26 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/28 03:04:27 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int		cam_put(t_camera c, t_scene *sc)
{
	t_obj		o;
	t_camera	*cam;

	if (!ensure_arr_size(&(sc->cams)))
		return (0);
	cam = malloc(sizeof(t_camera));
	if (cam == NULL)
		return (!put_error(ERR_ALLOC));
	c.vup = (t_vect){0, 1, 0};
	if (fabs(v_scalar(c.vup, c.r.dir)) > 1.0 - EPSILON)
		c.vup = (t_vect){0, 0, -1};
	c.anaglyph /= 2;
	c.right = v_unit(v_cross(c.r.dir, c.vup));
	*cam = c;
	o.type = OBJ_CAMERA;
	o.mat = (t_material){0};
	o.obj = (void*)cam;
	sc->cams.arr[sc->cams.len] = o;
	sc->cams.len += 1;
	return (1);
}

int		cam_read(char **words, t_scene *sc, int lineno)
{
	t_camera c;

	c.f = (t_filter){0, NULL};
	c.anaglyph = 0;
	if (ensure_args_range(words, 4, 6, lineno) &&
		v_read(&(c.r.origin), words[1], lineno, VECMODE_ANY) &&
		v_read(&(c.r.dir), words[2], lineno, VECMODE_NORM) &&
		read_uint(&(c.fov), words[3], lineno, INTMODE_ANGLE) &&
		(words[4] == NULL || (read_double(&(c.anaglyph), words[4], lineno,
			DBLMODE_ANY) &&
		(words[5] == NULL || read_filter(&(c.f), words[5], lineno)))) &&
		cam_put(c, sc))
		return (1);
	return (0);
}

void	cam_move(t_camera *cam, double dx, double dy, double dz)
{
	cam->r.origin = v_add4(
		cam->r.origin,
		v_factor(cam->right, dx),
		v_factor(cam->vup, dy),
		v_factor(cam->r.dir, dz));
}

void	cam_rotate(t_camera *cam, double rx, double ry, double rz)
{
	if (rx != 0)
	{
		cam->r.dir = v_unit(v_rotate(cam->r.dir, cam->right, rx));
		cam->vup = v_unit(v_cross(cam->right, cam->r.dir));
	}
	if (ry != 0)
	{
		cam->r.dir = v_unit(v_rotate(cam->r.dir, cam->vup, ry));
		cam->right = v_unit(v_cross(cam->r.dir, cam->vup));
	}
	if (rz != 0)
	{
		cam->vup = v_unit(v_rotate(cam->vup, cam->r.dir, rz));
		cam->right = v_unit(v_cross(cam->r.dir, cam->vup));
	}
}

int		cam_update(t_mlx_data *data, double move[], double rotate[])
{
	t_camera	*cam;
	t_scene		*sc;

	cam = (t_camera*)(data->scene.cams.arr[data->current].obj);
	sc = &(data->scene);
	cam_move(cam, move[0], move[1], move[2]);
	cam_rotate(cam, rotate[0], rotate[1], rotate[2]);
	return (rerender_threaded(sc, &(data->img), data->current) &&
		update_window(data));
}
