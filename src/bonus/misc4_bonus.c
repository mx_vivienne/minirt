/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   misc4_bonus.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/21 20:42:57 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/21 20:42:58 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

int		test_string(char *str, int (*test)(int))
{
	size_t	offset;

	offset = 0;
	while (str[offset] != '\0')
	{
		if (!test(str[offset]))
			return (0);
		offset++;
	}
	return (1);
}

double	test_hits(t_ray r, double a, double b)
{
	if (fabs(r_point(r, a).y) > 1.0)
		a = -1.0;
	if (fabs(r_point(r, b).y) > 1.0)
		b = -1.0;
	return (min_pos(a, b));
}

/*
** sadly only the linux version of mlx includes mlx_destroy_display
*/

#if COMPILE_FOR_MAC

int		rt_exit(t_mlx_data *data)
{
	data->current = 0;
	while (data->current < data->len)
	{
		free(data->img[data->current].data);
		data->current++;
	}
	free(data->img);
	objs_destroy(&(data->scene));
	mlx_destroy_image(data->ptrs.mlx, data->ptrs.img.img);
	mlx_destroy_window(data->ptrs.mlx, data->ptrs.win);
	free(data->ptrs.mlx);
	free(data);
	exit(0);
}

#else

int		rt_exit(t_mlx_data *data)
{
	data->current = 0;
	while (data->current < data->len)
	{
		free(data->img[data->current].data);
		data->current++;
	}
	free(data->img);
	objs_destroy(&(data->scene));
	mlx_destroy_image(data->ptrs.mlx, data->ptrs.img.img);
	mlx_destroy_window(data->ptrs.mlx, data->ptrs.win);
	mlx_destroy_display(data->ptrs.mlx);
	free(data->ptrs.mlx);
	free(data);
	exit(0);
}

#endif
