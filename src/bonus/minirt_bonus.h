/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minirt_bonus.h                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/07 22:21:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/12 20:39:45 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINIRT_BONUS_H
# define MINIRT_BONUS_H

# include <sys/types.h>
# include <sys/stat.h>
# include <pthread.h>
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <math.h>
# include "mlx.h"
# include "libft.h"
# include "errors_bonus.h"
# include "defines_bonus.h"
# include "typedefs_bonus.h"
# include "keycodes_bonus.h"

/*
** minirt.c
*/
int				main(int argc, char *argv[]);

/*
** vector1.c
*/
t_vect			v_add(t_vect u, t_vect v);
t_vect			v_subt(t_vect u, t_vect v);
t_vect			v_factor(t_vect u, double f);
t_vect			v_mult(t_vect u, t_vect v);
t_vect			v_div(t_vect u, double f);

/*
** vector2.c
*/
double			v_scalar(t_vect u, t_vect v);
t_vect			v_cross(t_vect u, t_vect v);
double			v_len(t_vect u);
t_vect			v_unit(t_vect u);
int				v_read(t_vect *u, char *params, int lineno, int mode);

/*
** vector3.c
*/
t_vect			v_add3(t_vect u, t_vect v, t_vect w);
t_vect			v_add4(t_vect u, t_vect v, t_vect w, t_vect x);
double			v_dist(t_vect u, t_vect v);
int				v_are_orthogonal(t_vect u, t_vect v);
int				v_exact_equals(t_vect u, t_vect v);

/*
** vector4.c
*/
t_vect			v_rotate(t_vect v, t_vect axis, double angle);

/*
** ray.c
*/
t_vect			r_point(t_ray r, double t);
t_ray			r_through(t_vect orig, t_vect point);
t_hit			r_trace(t_arr objs, t_ray ray, int do_normal, int do_dist);
t_color			r_color(t_scene sc, t_ray ray);
double			r_dist(t_ray r, double t);

/*
** matrix1.c
*/
t_matrix		m_add(t_matrix m, t_matrix n);
t_matrix		m_subt(t_matrix m, t_matrix n);
t_matrix		m_factor(t_matrix m, double f);
t_matrix		m_mult(t_matrix m, t_matrix n);
t_vect			m_apply(t_matrix m, t_vect v);

/*
** matrix2.c
*/
t_matrix		rotation_matrix(t_vect from, t_vect to);
t_matrix		stretch_matrix(t_vect factors);

/*
** color1.c
*/
t_color			c_add(t_color c1, t_color c2);
t_color			c_mult(t_color c1, t_color c2);
t_color			c_cap(t_color c);
t_color			c_shade(t_color c, double f);
int				c_read(t_color *c, char *params, int lineno);

/*
** color2.c
*/
t_color			c_add_uncapped(t_color c1, t_color c2);
t_color			c_div(t_color c, double f);
t_color			c_from_vect(t_vect v);

/*
** material.c
*/
int				mat_put(t_material mat, t_scene *sc, int lineno,
					t_material *ptr);
int				mat_read(char **words, t_scene *sc, int lineno);
void			mat_destroy(t_material *mat);
int				m_name_read(t_material *m, char *param, t_scene sc, int ln);

/*
** material_color.c
*/
int				mat_read_color(t_property *col, char *params, int lineno);

/*
** material_surface.c
*/
int				mat_read_surface(t_property *surf, char *params, int lineno);

/*
** rainbow.c
*/
t_color			rainbow_color(t_vect normal, t_obj obj, t_color base);

/*
** objs1.c
*/
int				obj_read(char **words, t_scene *sc, int ln);
double			obj_ray(t_ray r, t_obj o);
t_vect			obj_normal(t_vect p, t_obj o);
t_coords		obj_uv(t_vect p, t_obj o);

/*
** objs2.c
*/
t_color			obj_color(t_hit h);

/*
** light1.c
*/
int				l_put(t_light l, t_color c, t_scene *sc);
int				l_read(char **words, t_scene *sc, int lineno);
t_color			l_trace(t_scene sc, t_hit hit, t_color color, t_obj light);
t_color			lighting(t_scene sc, t_hit hit);

/*
** light2.c
*/
int				l2_put(t_light2 l, t_color c, t_scene *sc);
int				l2_read(char **words, t_scene *sc, int lineno);

/*
** filter.c
*/
t_color			filter_hue_rotate(t_color c, double angle);
t_color			filter_apply(t_color c, t_filter f, size_t x, size_t y);

/*
** camera1.c
*/
void			cam_render_one(t_camera c, t_scene *sc, t_pixbuf img,
					t_thrinfo thr);
void			cam_render(t_camera c, t_scene *sc, t_pixbuf img,
					t_thrinfo thr);
void			cam3d_render(t_camera c, t_scene *sc, t_pixbuf img,
					t_thrinfo thr);

/*
** camera2.c
*/
int				img_alloc(t_scene sc, t_pixbuf **img);
void			img_free(t_pixbuf **img, size_t amount);
int				cam_render_all(t_scene *sc, t_pixbuf **img, t_thrinfo thr);
int				read_filter(t_filter *f, char *word, int lineno);

/*
** camera3.c
*/
int				cam_put(t_camera c, t_scene *sc);
int				cam_read(char **words, t_scene *sc, int lineno);
void			cam_move(t_camera *cam, double dx, double dy, double dz);
void			cam_rotate(t_camera *cam, double rx, double ry, double rz);
int				cam_update(t_mlx_data *data, double move[], double rotate[]);

/*
** skybox.c
*/
int				sky_put(double size, t_material *mats, t_scene *sc);
int				sky_read(char **words, t_scene *sc, int lineno);

/*
** sphere.c
*/
int				sp_put(t_sphere sp, t_material mat, t_scene *sc);
double			sp_ray(t_ray r, t_sphere s);
int				sp_read(char **words, t_scene *sc, int lineno);
t_vect			sp_normal(t_vect p, t_obj o);
t_coords		sp_uv(t_vect p, t_obj o);

/*
** cylinder1.c
*/
int				cy_put(t_cyl cy, t_material mat, t_scene *sc, int capped);
double			cy_ray(t_ray r, t_cyl cy);
int				cy_read(char **words, t_scene *sc, int lineno);

/*
** cylinder2.c
*/
t_vect			cy_normal(t_vect p, t_obj o);
t_coords		cy_uv(t_vect p, t_obj o);

/*
** cone1.c
*/
int				co_put(t_cone co, t_material mat, t_scene *sc, int capped);
double			co_ray(t_ray r, t_cone co);
int				co_read(char **words, t_scene *sc, int lineno);

/*
** cone2.c
*/
t_coords		co_uv(t_vect p, t_obj o);
t_vect			co_normal(t_vect p, t_obj o);

/*
** cube.c
*/
int				cb_put(t_cube cb, t_material mat, t_scene *sc);
int				cb_calc_normals(t_cube *cb, int lineno);
int				cb_read(char **words, t_scene *sc, int lineno);

/*
** pyramid.c
*/
int				py_put(t_pyram py, t_material mat, t_scene *sc);
int				py_calc_points(t_pyram *py, int lineno);
int				py_read(char **words, t_scene *sc, int lineno);

/*
** plane.c
*/
int				pl_put(t_plane pl, t_material mat, t_scene *sc);
double			pl_ray(t_ray r, t_plane pl);
int				pl_read(char **words, t_scene *sc, int lineno);
t_vect			pl_normal(t_vect p, t_obj o);
t_coords		pl_uv(t_vect p, t_obj o);

/*
** square.c
*/
int				sq_put(t_square sq, t_material mat, t_scene *sc, int is_skybox);
double			sq_ray(t_ray r, t_square sq);
int				sq_read(char **words, t_scene *sc, int lineno);
t_vect			sq_normal(t_vect p, t_obj o);
t_coords		sq_uv(t_vect p, t_obj o);

/*
** triangle.c
*/
int				tr_put(t_tri tr, t_material mat, t_scene *sc);
double			tr_ray(t_ray r, t_tri tr);
int				tr_read(char **words, t_scene *sc, int lineno);

/*
** triangle2.c
*/
t_vect			tr_normal(t_vect p, t_obj o);
t_coords		tr_uv(t_vect p, t_obj o);

/*
** circle.c
*/
int				ci_put(t_circle ci, t_material mat, t_scene *sc);
double			ci_ray(t_ray r, t_circle ci);
int				ci_read(char **words, t_scene *sc, int lineno);
t_vect			ci_normal(t_vect p, t_obj o);
t_coords		ci_uv(t_vect p, t_obj o);

/*
** input1.c
*/
int				input_read(int fd, t_scene *sc);
int				line_read(char **words, t_scene *sc, int *flags);
void			words_destroy(char ***words);

/*
** input2.c
*/
int				read_dbl_arr(double **arr, char **args, int lineno, int mode);
int				read_double(double *d, char *str, int lineno, int mode);
int				read_uint(unsigned int *i, char *str, int lineno, int mode);
int				read_triplet(char ***triplet, char *str, int lineno);

/*
** input3.c
*/
int				test_double(double d, int lineno, int mode);
int				test_uint(unsigned int i, int lineno, int mode);
int				amb_read(char **words, t_scene *sc, int lineno);
int				res_read(char **words, t_scene *sc, int lineno);
int				aa_read(char **words, t_scene *sc, int lineno);

/*
** input4.c
*/
int				th_read(char **words, t_scene *sc, int lineno);
int				ensure_arguments(char **args, int required, int lineno);
int				ensure_args_range(char **args, int min, int max, int lineno);

/*
** bmp.c
*/
ssize_t			create_bmp(char *file, t_pixbuf img);

/*
** ppm.c
*/
int				read_ppm(t_pixbuf *img, int fd, int lineno);

/*
** pgm.c
*/
int				read_pgm(t_bumpmap *img, int fd, int lineno);

/*
** window.c
*/
void			mlx_img_set(t_mlx_img img, t_pixbuf buf);
int				update_window(t_mlx_data *data);
int				handle_keypress(int key, t_mlx_data *data);
t_mlx_data		*create_window(char *title, t_scene *sc, void *mlx);

/*
** threads.c
*/
long			render_threaded(t_scene *sc, t_pixbuf **img);
void			*thread_start(void *ptr);
long			rerender_threaded(t_scene *sc, t_pixbuf **img, size_t index);
void			*rerender_start(void *ptr);

/*
** misc1.c
*/
int				ensure_arr_size(t_arr *arr);
int				arr_resize(t_arr *arr, size_t size);
void			arr_destroy(t_arr *arr);
void			objs_destroy(t_scene *sc);
unsigned int	encode_str(char *str);

/*
** misc2.c
*/
char			*ft_basename(char *path, int keep_suffix);
char			*create_filename(char *name, char *suffix,
					size_t num, int use_num);
char			*create_winname(char *name, size_t num, int use_num);
double			min_pos(double a, double b);
size_t			uint_cycle(size_t num, size_t max, int up);

/*
** misc3.c
*/
int				next_word(int fd, char **tok, int lineno, int do_reset);
int				get_word(int fd, char **word, int lineno);
int				read_size(size_t *i, char *str, size_t max, int lineno);

/*
** misc4.c
*/
int				test_string(char *str, int (*test)(int));
double			test_hits(t_ray r, double a, double b);
int				rt_exit(t_mlx_data *data);

/*
** error.c
*/
int				put_error(char *msg);
int				put_error_line(char *msg, int line);
int				put_error_options(char *msg, int line, char *options[]);

#endif
