/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   window_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/30 19:51:03 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/02/07 23:11:13 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minirt_bonus.h"

void		mlx_img_set(t_mlx_img img, t_pixbuf buf)
{
	char	*row;
	size_t	x;
	size_t	y;
	t_color	c;

	y = 0;
	while (y < buf.h)
	{
		x = 0;
		row = img.ptr + y * img.linelen;
		while (x < buf.w)
		{
			c = buf.data[y * buf.w + x];
			*(unsigned int*)row = c.r << 16 | c.g << 8 | c.b;
			row += 4;
			x++;
		}
		y++;
	}
}

void		cap_at_screensize(t_scene *sc, void *mlx)
{
	int	w;
	int	h;

	mlx_get_screen_size(mlx, &w, &h);
	if ((size_t)w < sc->x)
		sc->x = (size_t)w;
	if ((size_t)h < sc->y)
		sc->y = (size_t)h;
}

int			update_window(t_mlx_data *data)
{
	mlx_img_set(data->ptrs.img, data->img[data->current]);
	mlx_put_image_to_window(data->ptrs.mlx, data->ptrs.win,
		data->ptrs.img.img, 0, 0);
	return (1);
}

int			handle_keypress(int key, t_mlx_data *data)
{
	if (key == KEY_ESC)
		rt_exit(data);
	if (data->len > 1 && (key == KEY_LEFT || key == KEY_RIGHT))
	{
		data->current = uint_cycle(data->current, data->len - 1,
			key == KEY_RIGHT);
		update_window(data);
	}
	if (key == KEY_W || key == KEY_A || key == KEY_S || key == KEY_D ||
		key == KEY_E || key == KEY_Q || key == KEY_1 || key == KEY_2 ||
		key == KEY_3 || key == KEY_4 || key == KEY_5 || key == KEY_6)
	{
		cam_update(data, (double[3]){
			.1 * ((key == KEY_D) - (key == KEY_A)),
			.1 * ((key == KEY_E) - (key == KEY_Q)),
			.1 * ((key == KEY_W) - (key == KEY_S))}, (double[]){
			.1 * ((key == KEY_1) - (key == KEY_2)),
			.1 * ((key == KEY_3) - (key == KEY_4)),
			.1 * ((key == KEY_5) - (key == KEY_6))});
	}
	return (1);
}

t_mlx_data	*create_window(char *title, t_scene *sc, void *mlx)
{
	t_mlx_data	*data;
	t_mlx_img	mi;

	if (title == NULL)
		return (NULL);
	data = malloc(sizeof(t_mlx_data));
	if (data == NULL)
	{
		put_error(ERR_ALLOC);
		return (NULL);
	}
	cap_at_screensize(sc, mlx);
	data->ptrs.mlx = mlx;
	data->ptrs.win = mlx_new_window(data->ptrs.mlx, sc->x, sc->y, title);
	mi.img = mlx_new_image(data->ptrs.mlx, sc->x, sc->y);
	mi.ptr = mlx_get_data_addr(mi.img, &(mi.bpp), &(mi.linelen), &(mi.endian));
	data->ptrs.img = mi;
	data->scene = *sc;
	mlx_hook(data->ptrs.win, EVT_VIS, MASK_VIS, &update_window, data);
	mlx_hook(data->ptrs.win, EVT_CLOSE, 0, &rt_exit, data);
	mlx_key_hook(data->ptrs.win, &handle_keypress, data);
	return (data);
}
