/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   material_surface_bonus.c                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/20 03:11:41 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/20 03:11:43 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static int	g_max_len = 9;

static char	*g_surf_keywords[] = {
	"regular", "sine", "normalmap", NULL
};

static int	mat_surf_args(t_property *surf, char **words, int lineno)
{
	surf->data = NULL;
	if (surf->type == M_SURF_REGULAR || surf->type == M_SURF_SINE)
		return (ensure_arguments(words, 1, lineno));
	return (0);
}

static int	mat_surf_type(t_property *surf, char **words, int lineno)
{
	size_t	offset;

	offset = 0;
	while (1)
	{
		if (g_surf_keywords[offset] == NULL)
			return (!put_error_options(ERR_KEYWORD, lineno, g_surf_keywords));
		if (!ft_strncmp(words[0], g_surf_keywords[offset], g_max_len))
		{
			surf->type = offset;
			return (1);
		}
		offset++;
	}
}

int			mat_read_surface(t_property *surf, char *params, int lineno)
{
	int		ret;
	char	**words;

	words = ft_split(params, ':');
	if (words[0] == NULL)
	{
		words_destroy(&words);
		return (!put_error_line(ERR_FEWARGS, lineno));
	}
	surf->scale = 1;
	ret = mat_surf_type(surf, words, lineno) &&
		mat_surf_args(surf, words, lineno);
	words_destroy(&words);
	return (ret);
}
