/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   camera2_bonus.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/31 17:42:28 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/31 17:42:30 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

static char		*g_filters[] = {
	"none", "sepia", "greyscale", "invert", "hueshift", "scanline", NULL};

static int		g_filter_args[] = {1, 1, 1, 1, 2, 2};

int				img_alloc(t_scene sc, t_pixbuf **img)
{
	size_t	offset;

	*img = malloc(sc.cams.len * sizeof(t_pixbuf));
	if (*img == NULL)
		return (!put_error(ERR_ALLOC));
	offset = 0;
	while (offset < sc.cams.len)
	{
		(*img)[offset] = (t_pixbuf){sc.x, sc.y,
			malloc(sc.x * sc.y * sizeof(t_color))};
		if ((*img)[offset].data == NULL)
		{
			img_free(img, offset);
			return (!put_error(ERR_ALLOC));
		}
		offset++;
	}
	return (1);
}

void			img_free(t_pixbuf **img, size_t amount)
{
	size_t	offset;

	offset = 0;
	while (offset < amount)
	{
		free((*img)[offset].data);
		offset++;
	}
	free(*img);
	*img = NULL;
}

int				cam_render_all(t_scene *sc, t_pixbuf **img, t_thrinfo thr)
{
	size_t		offset;
	t_camera	cam;

	if (img == NULL || *img == NULL)
		return (0);
	offset = 0;
	while (offset < sc->cams.len)
	{
		cam = *(t_camera*)sc->cams.arr[offset].obj;
		if (cam.anaglyph != 0)
			cam3d_render(cam, sc, (*img)[offset], thr);
		else
			cam_render(cam, sc, (*img)[offset], thr);
		offset++;
	}
	return (1);
}

static int		find_filter_index(char *filter, int *index, int lineno)
{
	size_t	offset;

	offset = 0;
	while (g_filters[offset] != NULL)
	{
		if (0 == ft_strncmp(filter, g_filters[offset],
			ft_strlen(g_filters[offset])))
		{
			*index = offset;
			return (1);
		}
		offset++;
	}
	return (!put_error_options(ERR_KEYWORD, lineno, g_filters));
}

int				read_filter(t_filter *f, char *word, int lineno)
{
	char	**args;

	args = ft_split(word, ':');
	if (args == NULL || !find_filter_index(args[0], &(f->type), lineno) ||
		!ensure_arguments(args, g_filter_args[f->type], lineno))
	{
		words_destroy(&args);
		return (0);
	}
	if (g_filter_args[f->type] > 1)
	{
		f->args = malloc(sizeof(double) * (g_filter_args[f->type] - 1));
		if (f->args == NULL ||
			!read_dbl_arr(&(f->args), args + 1, lineno, DBLMODE_ANY))
		{
			if (f->args == NULL)
				put_error(ERR_ALLOC);
			words_destroy(&args);
			free(f->args);
			return (0);
		}
	}
	words_destroy(&args);
	return (1);
}
