/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   color2_bonus.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/14 05:11:53 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/14 05:11:54 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

t_color	c_add_uncapped(t_color c1, t_color c2)
{
	return ((t_color){c1.r + c2.r, c1.g + c2.g, c1.b + c2.b});
}

t_color	c_div(t_color c, double f)
{
	return (c_cap((t_color){c.r / f, c.g / f, c.b / f}));
}

t_color	c_from_vect(t_vect v)
{
	v = v_unit(v);
	return ((t_color){
		(unsigned)fabs((v.x + 1) / 2 * 255),
		(unsigned)fabs((v.y + 1) / 2 * 255),
		(unsigned)fabs((v.z + 1) / 2 * 255)
	});
}
