/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   keycodes_bonus.h                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/02/05 22:12:19 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/02/07 23:10:32 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYCODES_BONUS_H
# define KEYCODES_BONUS_H

# if COMPILE_FOR_MAC

#  define KEY_LEFT		123
#  define KEY_RIGHT		124
#  define KEY_DOWN		125
#  define KEY_UP		126
#  define KEY_ESC		53

#  define KEY_Q			12
#  define KEY_W			13
#  define KEY_E			14
#  define KEY_A			0
#  define KEY_S			1
#  define KEY_D			2

#  define KEY_0			29
#  define KEY_1			18
#  define KEY_2			19
#  define KEY_3			20
#  define KEY_4			21
#  define KEY_5			23
#  define KEY_6			22
#  define KEY_7			26
#  define KEY_8			28
#  define KEY_9			25

#  define EVT_CLOSE		17
#  define EVT_VIS		15
#  define MASK_VIS		65536L

# else

#  define KEY_LEFT		0xff51
#  define KEY_RIGHT		0xff53
#  define KEY_DOWN		0xff54
#  define KEY_UP		0xff52
#  define KEY_ESC		0xff1b

#  define KEY_Q			0x71
#  define KEY_W			0x77
#  define KEY_E			0x65
#  define KEY_A			0x61
#  define KEY_S			0x73
#  define KEY_D			0x64

#  define KEY_0			0x30
#  define KEY_1			0x31
#  define KEY_2			0x32
#  define KEY_3			0x33
#  define KEY_4			0x34
#  define KEY_5			0x35
#  define KEY_6			0x36
#  define KEY_7			0x37
#  define KEY_8			0x38
#  define KEY_9			0x39

#  define EVT_CLOSE		33
#  define EVT_VIS		15
#  define MASK_VIS		65536L

# endif
#endif
