/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   misc2_bonus.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/23 19:00:19 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/23 19:00:20 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt_bonus.h"

char	*ft_basename(char *path, int keep_suffix)
{
	size_t	offset;
	char	*start;
	char	*end;

	offset = 0;
	start = path;
	while (path[offset] != '\0')
	{
		if (path[offset] == '/')
			start = path + offset + 1;
		offset++;
	}
	if (!keep_suffix)
	{
		end = path + offset;
		offset = 0;
		while (start[offset] != '\0')
		{
			if (start[offset] == '.')
				end = start + offset;
			offset++;
		}
		*end = '\0';
	}
	return (start);
}

char	*create_filename(char *name, char *suffix, size_t num, int use_num)
{
	char	*str;
	char	*n;
	size_t	len;

	n = ft_itoa(num);
	name = ft_basename(name, 0);
	len = ft_strlen(name) + ft_strlen(suffix) + !!use_num * ft_strlen(n) + 1;
	str = malloc(len);
	if (str == NULL)
	{
		free(n);
		put_error(ERR_ALLOC);
		return (NULL);
	}
	ft_strlcpy(str, name, len);
	if (use_num)
		ft_strlcat(str, n, len);
	ft_strlcat(str, suffix, len);
	free(n);
	return (str);
}

char	*create_winname(char *name, size_t num, int use_num)
{
	char	*str;
	char	*n;
	size_t	len;

	n = ft_itoa(num);
	name = ft_basename(name, 1);
	len = ft_strlen(name) + ft_strlen(NAME) + 4 +
		!!use_num * (ft_strlen(n) + 3);
	str = malloc(len);
	if (str == NULL)
		put_error(ERR_ALLOC);
	else
	{
		ft_strlcpy(str, NAME, len);
		ft_strlcat(str, " - ", len);
		ft_strlcat(str, name, len);
		if (use_num)
		{
			ft_strlcat(str, " (", len);
			ft_strlcat(str, n, len);
			ft_strlcat(str, ")", len);
		}
	}
	free(n);
	return (str);
}

double	min_pos(double a, double b)
{
	if (a < 0.0)
		return (b);
	if (b < 0.0 || a < b)
		return (a);
	return (b);
}

size_t	uint_cycle(size_t num, size_t max, int up)
{
	if (num == 0 && !up)
		return (max);
	if (num == max && up)
		return (0);
	return (!!up * 2 - 1 + num);
}
