/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minirt.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/07 22:21:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/12 20:39:45 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINIRT_H
# define MINIRT_H

# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <math.h>
# include "mlx.h"
# include "errors.h"
# include "defines.h"
# include "typedefs.h"
# include "../libft/libft.h"

/*
** minirt.c
*/
int				main(int argc, char *argv[]);

/*
** vector1.c
*/
t_vect			v_add(t_vect u, t_vect v);
t_vect			v_subt(t_vect u, t_vect v);
t_vect			v_factor(t_vect u, double f);
t_vect			v_mult(t_vect u, t_vect v);
t_vect			v_div(t_vect u, double f);

/*
** vector2.c
*/
double			v_scalar(t_vect u, t_vect v);
t_vect			v_cross(t_vect u, t_vect v);
double			v_len(t_vect u);
t_vect			v_unit(t_vect u);
int				v_read(t_vect *u, char *params, int lineno, int mode);

/*
** vector3.c
*/
t_vect			v_add3(t_vect u, t_vect v, t_vect w);
t_vect			v_add4(t_vect u, t_vect v, t_vect w, t_vect x);
double			v_dist(t_vect u, t_vect v);

/*
** ray1.c
*/
t_vect			r_point(t_ray r, double t);
t_ray			r_through(t_vect orig, t_vect point);
t_hit			r_trace(t_arr objs, t_ray ray, int do_normal, int do_dist);
t_color			r_color(t_scene sc, t_ray ray);
double			r_dist(t_ray r, double t);

/*
** matrix1.c
*/
t_matrix		m_add(t_matrix m, t_matrix n);
t_matrix		m_subt(t_matrix m, t_matrix n);
t_matrix		m_factor(t_matrix m, double f);
t_matrix		m_mult(t_matrix m, t_matrix n);
t_vect			m_apply(t_matrix m, t_vect v);

/*
** matrix2.c
*/
t_matrix		rotation_matrix(t_vect from, t_vect to);
t_matrix		stretch_matrix(t_vect factors);

/*
** color1.c
*/
t_color			c_add(t_color c1, t_color c2);
t_color			c_mult(t_color c1, t_color c2);
t_color			c_cap(t_color c);
t_color			c_shade(t_color c, double f);
int				c_read(t_color *c, char *params, int lineno);

/*
** objs1.c
*/
int				obj_read(char **words, t_scene *sc, int ln);
double			obj_ray(t_ray r, t_obj o);
t_vect			obj_normal(t_vect p, t_obj o);

/*
** light1.c
*/
int				l_put(t_light l, t_color c, t_scene *sc);
int				l_read(char **words, t_scene *sc, int lineno);
t_color			l_trace(t_scene sc, t_hit hit, t_obj light);
t_color			lighting(t_scene sc, t_hit hit);

/*
** camera1.c
*/
t_pixbuf		cam_render(t_camera c, t_scene *sc);
int				cam_put(t_camera c, t_scene *sc);
int				cam_read(char **words, t_scene *sc, int lineno);

/*
** camera2.c
*/
int				cam_render_all(t_scene *sc, t_pixbuf **img);

/*
** sphere1.c
*/
int				sp_put(t_sphere sp, t_color c, t_scene *sc);
double			sp_ray(t_ray r, t_sphere s);
int				sp_read(char **words, t_scene *sc, int lineno);

/*
** cylinder1.c
*/
int				cy_put(t_cyl cy, t_color c, t_scene *sc);
double			cy_ray(t_ray r, t_cyl cy);
int				cy_read(char **words, t_scene *sc, int lineno);

/*
** plane1.c
*/
int				pl_put(t_plane pl, t_color c, t_scene *sc);
double			pl_ray(t_ray r, t_plane pl);
int				pl_read(char **words, t_scene *sc, int lineno);

/*
** square1.c
*/
int				sq_put(t_square sq, t_color c, t_scene *sc);
double			sq_ray(t_ray r, t_square sq);
int				sq_read(char **words, t_scene *sc, int lineno);

/*
** triangle1.c
*/
int				tr_put(t_tri tr, t_color c, t_scene *sc);
double			tr_ray(t_ray r, t_tri tr);
int				tr_read(char **words, t_scene *sc, int lineno);

/*
** circle1.c
*/
int				ci_put(t_circle ci, t_color c, t_scene *sc);
double			ci_ray(t_ray r, t_circle ci);
int				ci_read(char **words, t_scene *sc, int lineno);

/*
** input1.c
*/
int				input_read(int fd, t_scene *sc);
int				line_read(char **words, t_scene *sc, int *flags);
int				ensure_arguments(char **args, int required, int lineno);
void			words_destroy(char ***words);

/*
** input2.c
*/
int				read_double(double *d, char *str, int lineno, int mode);
int				read_uint(unsigned int *i, char *str, int lineno, int mode);
int				test_double(double d, int lineno, int mode);
int				test_uint(unsigned int i, int lineno, int mode);

/*
** input3.c
*/
int				amb_read(char **words, t_scene *sc, int lineno);
int				res_read(char **words, t_scene *sc, int lineno);
int				read_triplet(char ***triplet, char *str, int lineno);

/*
** bmp.c
*/
ssize_t			create_bmp(char *file, t_pixbuf img);

/*
** window1.c
*/
void			mlx_img_set(t_mlx_img img, t_pixbuf buf);
int				update_window(t_mlx_data *data);
int				handle_keypress(int key, t_mlx_data *data);
t_mlx_data		*create_window(char *title, t_scene *sc, void *mlx);

/*
** misc1.c
*/
int				ensure_arr_size(t_arr *arr);
int				arr_resize(t_arr *arr, size_t size);
void			arr_destroy(t_arr *arr);
void			objs_destroy(t_scene *sc);
unsigned int	encode_str(char *str);

/*
** misc2.c
*/
char			*ft_basename(char *path, int keep_suffix);
char			*create_filename(char *name, char *suffix,
					size_t num, int use_num);
char			*create_winname(char *name, size_t num, int use_num);
double			min_pos(double a, double b);
size_t			uint_cycle(size_t num, size_t max, int up);

/*
** misc3.c
*/
int				rt_exit(t_mlx_data *data);

/*
** error1.c
*/
int				put_error(char *msg);
int				put_error_line(char *msg, int line);

#endif
