/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   vector2.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/10 02:39:20 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/10 19:35:36 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

double	v_scalar(t_vect u, t_vect v)
{
	return (u.x * v.x +
			u.y * v.y +
			u.z * v.z);
}

t_vect	v_cross(t_vect u, t_vect v)
{
	return ((t_vect){
			u.y * v.z - u.z * v.y,
			u.z * v.x - u.x * v.z,
			u.x * v.y - u.y * v.x});
}

double	v_len(t_vect u)
{
	return (sqrt(
			u.x * u.x +
			u.y * u.y +
			u.z * u.z));
}

t_vect	v_unit(t_vect u)
{
	return (v_div(u, v_len(u)));
}

int		v_read(t_vect *u, char *params, int lineno, int mode)
{
	char	**coords;
	int		ret;

	if (!read_triplet(&coords, params, lineno))
		return (0);
	ret = 1;
	if (!read_double(&(u->x), coords[0], lineno, mode) ||
		!read_double(&(u->y), coords[1], lineno, mode) ||
		!read_double(&(u->z), coords[2], lineno, mode))
		ret = 0;
	words_destroy(&coords);
	if (ret && (mode & VECMODE_NORM) &&
		fabs(v_len(*u) - 1.0) > EPSILON)
		ret = !put_error_line(ERR_VNONNORM, lineno);
	return (ret);
}
