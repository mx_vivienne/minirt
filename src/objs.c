/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   objs.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/16 17:26:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/16 17:26:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static unsigned int	g_readtypes[] = {
	OBJ_SPHERE, OBJ_CYLINDER, OBJ_TRIANGLE, OBJ_SQUARE,
	OBJ_PLANE, OBJ_LIGHT, OBJ_CAMERA, OBJ_AMBIENT, OBJ_RESOLUTION, 0
};

static int			(*g_readfuncs[])(char**, t_scene*, int) = {
	sp_read, cy_read, tr_read, sq_read,
	pl_read, l_read, cam_read, amb_read, res_read
};

int					obj_read(char **words, t_scene *sc, int ln)
{
	unsigned int	type;
	int				offset;

	offset = 0;
	type = encode_str(words[0]);
	while (g_readtypes[offset] != 0)
	{
		if (type == g_readtypes[offset])
			return ((g_readfuncs[offset](words, sc, ln)));
		offset++;
	}
	return (!put_error_line(ERR_INVTYPE, ln));
}

double				obj_ray(t_ray r, t_obj o)
{
	if (o.type == OBJ_SPHERE)
		return (sp_ray(r, *(t_sphere*)o.obj));
	if (o.type == OBJ_CYLINDER)
		return (cy_ray(r, *(t_cyl*)o.obj));
	if (o.type == OBJ_TRIANGLE)
		return (tr_ray(r, *(t_tri*)o.obj));
	if (o.type == OBJ_SQUARE)
		return (sq_ray(r, *(t_square*)o.obj));
	if (o.type == OBJ_CIRCLE)
		return (ci_ray(r, *(t_circle*)o.obj));
	if (o.type == OBJ_PLANE)
		return (pl_ray(r, *(t_plane*)o.obj));
	return (-1.0);
}

t_vect				obj_normal(t_vect p, t_obj o)
{
	if (o.type == OBJ_SPHERE)
		return (v_unit(v_subt(p, (*(t_sphere*)o.obj).center)));
	if (o.type == OBJ_CYLINDER)
	{
		return (v_unit(v_cross((*(t_cyl*)o.obj).up,
			v_cross(v_subt(p, (*(t_cyl*)o.obj).center), (*(t_cyl*)o.obj).up))));
	}
	if (o.type == OBJ_TRIANGLE)
		return ((*(t_tri*)o.obj).normal);
	if (o.type == OBJ_SQUARE)
		return ((*(t_square*)o.obj).normal);
	if (o.type == OBJ_CIRCLE)
		return ((*(t_circle*)o.obj).normal);
	if (o.type == OBJ_PLANE)
		return ((*(t_plane*)o.obj).normal);
	return ((t_vect){0, 0, 0});
}
