/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   input3.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/14 03:19:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/14 03:19:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int		amb_read(char **words, t_scene *sc, int lineno)
{
	double	intensity;

	if (ensure_arguments(words, 3, lineno) &&
		read_double(&intensity, words[1], lineno, DBLMODE_POS | DBLMODE_UNIT) &&
		c_read(&(sc->amb), words[2], lineno))
	{
		sc->amb = c_shade(sc->amb, intensity);
		return (1);
	}
	return (0);
}

int		res_read(char **words, t_scene *sc, int lineno)
{
	if (ensure_arguments(words, 3, lineno) &&
		read_uint((unsigned*)&(sc->x), words[1], lineno, INTMODE_NOZERO) &&
		read_uint((unsigned*)&(sc->y), words[2], lineno, INTMODE_NOZERO))
		return (1);
	return (0);
}

int		read_triplet(char ***triplet, char *str, int lineno)
{
	*triplet = ft_split(str, ',');
	if (!ensure_arguments(*triplet, 3, lineno))
	{
		words_destroy(triplet);
		return (0);
	}
	return (1);
}
