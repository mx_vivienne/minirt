/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   camera2.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/31 17:42:28 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/31 17:42:30 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static void	img_free(t_pixbuf **img, size_t amount)
{
	size_t	offset;

	offset = 0;
	while (offset < amount)
	{
		free((*img)[offset].data);
		offset++;
	}
	free(*img);
	*img = NULL;
}

int			cam_render_all(t_scene *sc, t_pixbuf **img)
{
	size_t	offset;

	if (img == NULL || *img == NULL)
		return (0);
	offset = 0;
	while (offset < sc->cams.len)
	{
		(*img)[offset] = cam_render(*(t_camera*)(sc->cams.arr[offset].obj), sc);
		if ((*img)[offset].data == NULL)
		{
			img_free(img, offset);
			return (0);
		}
		offset++;
	}
	return (1);
}
