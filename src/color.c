/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   color.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/12 19:46:14 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/12 19:46:15 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

t_color	c_add(t_color c1, t_color c2)
{
	c1.r += c2.r;
	c1.g += c2.g;
	c1.b += c2.b;
	return (c_cap(c1));
}

t_color	c_mult(t_color c1, t_color c2)
{
	c1.r = (unsigned)((double)c1.r / 255.0 * c2.r);
	c1.g = (unsigned)((double)c1.g / 255.0 * c2.g);
	c1.b = (unsigned)((double)c1.b / 255.0 * c2.b);
	return (c1);
}

t_color	c_cap(t_color c)
{
	if (c.r > 255)
		c.r = 255;
	if (c.g > 255)
		c.g = 255;
	if (c.b > 255)
		c.b = 255;
	return (c);
}

t_color	c_shade(t_color c, double f)
{
	f = fmin(1.0, f);
	c.r = (unsigned)(f * (double)c.r);
	c.g = (unsigned)(f * (double)c.g);
	c.b = (unsigned)(f * (double)c.b);
	return (c);
}

int		c_read(t_color *c, char *params, int lineno)
{
	char	**triplet;
	int		mode;

	if (!read_triplet(&triplet, params, lineno))
		return (0);
	mode = INTMODE_POS | INTMODE_BYTE;
	if (!read_uint(&(c->r), triplet[0], lineno, mode) ||
		!read_uint(&(c->g), triplet[1], lineno, mode) ||
		!read_uint(&(c->b), triplet[2], lineno, mode))
	{
		words_destroy(&triplet);
		return (0);
	}
	words_destroy(&triplet);
	return (1);
}
