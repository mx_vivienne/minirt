/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   errors.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/12 20:45:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/12 20:45:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERRORS_H
# define ERRORS_H

# define ERR_ERROR		"Error"
# define ERR_LINE		" on line "

# define ERR_USAGE		"usage: ./miniRT file [--save]"
# define ERR_FILEEXT	"invalid file, needs .rt extension"
# define ERR_FILEREAD	"could not read file"
# define ERR_LINEREAD	"could not read line"
# define ERR_NEWFILE	"could not open file for writing"
# define ERR_ALLOC		"memory allocation failed"
# define ERR_RESIZE		"resizing object array failed"
# define ERR_ARRBUG		"bug in memory management detected"

# define ERR_MLXINIT	"mlx initialization failed"

# define ERR_MANYARGS	"too many arguments"
# define ERR_FEWARGS	"too few arguments"
# define ERR_INVARGS	"invalid argument"
# define ERR_MISSARG	"missing argument"
# define ERR_INVTYPE	"unknown type identifier"
# define ERR_NORES		"resolution missing"
# define ERR_NOAMB		"ambient lighting missing"
# define ERR_NOCAMERA	"camera missing"
# define ERR_DUPINFO	"unique type identifier redefined"

# define ERR_MISSARG	"missing argument"
# define ERR_INVINT		"illegal char in int argument"
# define ERR_NEGINT		"illegal negative int"
# define ERR_ZEROINT	"illegal int 0"
# define ERR_INVBYTE	"int too large (max: 255)"
# define ERR_INVANGLE	"int too large (max: 180)"

# define ERR_INVDBL		"illegal char in float argument"
# define ERR_NEGDBL		"illegal negative float"
# define ERR_ZERODBL	"illegal float 0"
# define ERR_SMALLDBL	"float too small (min: -1.0)"
# define ERR_LARGEDBL	"float too large (max: 1.0)"

# define ERR_VNONNORM	"vector arg not normalized"

#endif
