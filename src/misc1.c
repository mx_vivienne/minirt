/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   misc1.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/13 16:55:16 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/13 16:55:18 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int				ensure_arr_size(t_arr *arr)
{
	if (arr->len < arr->mem)
		return (1);
	if (arr->len > arr->mem)
		return (!put_error(ERR_ARRBUG));
	if (!arr_resize(arr, arr->mem + OBJS_BUFFER))
		return (0);
	return (1);
}

int				arr_resize(t_arr *arr, size_t size)
{
	t_obj	*objs;
	size_t	offset;

	if (size == arr->mem)
		return (1);
	objs = malloc(size * sizeof(t_obj));
	if (objs == NULL)
		return (!put_error(ERR_RESIZE));
	offset = 0;
	while (offset < size && offset < arr->len)
	{
		objs[offset] = arr->arr[offset];
		offset++;
	}
	free(arr->arr);
	*arr = (t_arr){offset, size, objs};
	return (1);
}

void			arr_destroy(t_arr *arr)
{
	size_t	offset;

	offset = 0;
	while (offset < arr->len)
	{
		free(arr->arr[offset].obj);
		offset++;
	}
	free(arr->arr);
	arr->len = 0;
	arr->mem = 0;
	arr->arr = NULL;
}

void			objs_destroy(t_scene *sc)
{
	arr_destroy(&(sc->objs));
	arr_destroy(&(sc->cams));
}

unsigned int	encode_str(char *str)
{
	int				offset;
	unsigned int	code;

	offset = 0;
	code = 0;
	while (offset < 4 && str[offset] != '\0')
	{
		code |= (unsigned)str[offset] << offset * 8;
		offset++;
	}
	return (code);
}
