/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   circle.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/27 16:27:36 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/27 16:27:40 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "typedefs.h"

int				ci_put(t_circle ci, t_color c, t_scene *sc)
{
	t_obj		o;
	t_circle	*circ;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	circ = malloc(sizeof(t_circle));
	if (circ == NULL)
		return (!put_error(ERR_ALLOC));
	ci.radius /= 2;
	*circ = ci;
	o.type = OBJ_CIRCLE;
	o.color = c;
	o.obj = (void*)circ;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

double			ci_ray(t_ray r, t_circle ci)
{
	double	d;

	d = v_scalar(r.dir, ci.normal);
	if (fabs(d) < EPSILON)
		return (-1.0);
	d = v_scalar(v_subt(ci.center, r.origin), ci.normal) / d;
	if (d >= 0.0 && v_len(v_subt(r_point(r, d), ci.center)) <= ci.radius)
		return (d);
	return (-1.0);
}

int				ci_read(char **words, t_scene *sc, int lineno)
{
	t_circle	ci;
	t_color		c;

	if (ensure_arguments(words, 5, lineno) &&
		v_read(&(ci.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(ci.normal), words[2], lineno, VECMODE_NORM) &&
		read_double(&(ci.radius), words[3], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		c_read(&c, words[4], lineno) &&
		ci_put(ci, c, sc))
		return (1);
	return (0);
}
