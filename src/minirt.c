/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minirt.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/09 17:33:32 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/12 19:17:39 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int	main_bmp(char *rtname, t_scene scene)
{
	t_pixbuf	img;
	char		*filename;
	size_t		i;

	i = 0;
	img = (t_pixbuf){0, 0, NULL};
	while (i < scene.cams.len)
	{
		filename = create_filename(rtname, ".bmp", i + 1, scene.cams.len > 1);
		if (filename == NULL)
			return (1);
		img = cam_render(*(t_camera*)(scene.cams.arr[i].obj), &scene);
		if (img.data == NULL)
		{
			free(filename);
			return (1);
		}
		create_bmp(filename, img);
		free(filename);
		free(img.data);
		i++;
	}
	return (0);
}

static int	m_free(t_mlx_data **data, char **winname)
{
	if (winname != NULL)
	{
		free(*winname);
		*winname = NULL;
	}
	if (*data != NULL)
	{
		free((*data)->img);
		free((*data)->ptrs.mlx);
		free((*data)->ptrs.win);
		free((*data)->ptrs.img.img);
		free((*data)->ptrs.img.ptr);
		free(*data);
		*data = NULL;
	}
	return (0);
}

static int	m_init(t_mlx_data **data, t_scene sc, char *rtname)
{
	void		*mlx;
	char		*winname;
	t_pixbuf	*img;

	mlx = mlx_init();
	if (mlx == NULL)
		return (!put_error(ERR_MLXINIT));
	winname = create_winname(rtname, 0, 0);
	*data = create_window(winname, &sc, mlx);
	img = malloc(sc.cams.len * sizeof(t_pixbuf));
	if (img == NULL)
		return (!put_error(ERR_ALLOC) + m_free(data, NULL));
	if (*data == NULL || !cam_render_all(&sc, &img))
		return (!m_free(data, &winname));
	(*data)->img = img;
	(*data)->len = sc.cams.len;
	(*data)->current = 0;
	update_window(*data);
	free(winname);
	return (1);
}

static int	main_scene(char *rtname, t_scene scene, int to_bmp)
{
	t_mlx_data	*data;
	int			ret;

	ret = 0;
	if (to_bmp)
		ret = main_bmp(rtname, scene);
	else
	{
		if (!m_init(&data, scene, rtname))
			ret = 1;
		else
			mlx_loop(data->ptrs.mlx);
	}
	objs_destroy(&scene);
	return (ret);
}

int			main(int argc, char *argv[])
{
	t_scene		scene;
	int			fd;

	if (!(argc == 2 || (argc == 3 && !ft_strncmp("--save", argv[2], 7))))
		return (put_error(ERR_USAGE));
	if (ft_strlen(ft_basename(argv[1], 1)) < 4 ||
		ft_strncmp(argv[1] + ft_strlen(argv[1]) - 3, ".rt", 4))
		return (put_error(ERR_FILEEXT));
	scene = (t_scene){0, 0, {0}, {0}, {0}};
	fd = open(argv[1], O_RDONLY);
	if (!input_read(fd, &scene))
	{
		objs_destroy(&scene);
		close(fd);
		return (1);
	}
	close(fd);
	return (main_scene(argv[1], scene, argc == 3));
}
