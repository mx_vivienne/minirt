/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   input1.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/07 22:21:30 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/07 22:24:15 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int	input_read_loop(int fd, t_scene *sc, int *flags)
{
	int		ret;
	char	*line;
	char	**words;

	ret = get_next_line(fd, &line);
	while (ret > 0)
	{
		words = ft_split(line, ' ');
		free(line);
		if (!line_read(words, sc, flags))
		{
			objs_destroy(sc);
			return (0);
		}
		ret = get_next_line(fd, &line);
	}
	if (ret == 0)
	{
		free(line);
		return (1);
	}
	objs_destroy(sc);
	return (!put_error(ERR_FILEREAD));
}

int			input_read(int fd, t_scene *sc)
{
	int	flags;

	flags = 0;
	if (fd < 0)
		return (!put_error(ERR_FILEREAD));
	flags = 0;
	if (!input_read_loop(fd, sc, &flags))
		return (0);
	if (!(flags & FLAG_RES))
		return (!put_error(ERR_NORES));
	if (!(flags & FLAG_AMB))
		return (!put_error(ERR_NOAMB));
	if (sc->cams.arr == NULL)
		return (!put_error(ERR_NOCAMERA));
	return (1);
}

int			line_read(char **words, t_scene *sc, int *flags)
{
	static int	lineno = 1;
	int			ret;
	int			bit;

	if (words == NULL)
		return (!put_error_line(ERR_LINEREAD, lineno));
	ret = *words == NULL || obj_read(words, sc, lineno);
	if (ret && *words != NULL && ft_isupper(words[0][0]))
	{
		bit = FLAG_RES;
		if (OBJ_AMBIENT == encode_str(words[0]))
			bit = FLAG_AMB;
		if (bit & *flags)
			ret = !put_error_line(ERR_DUPINFO, lineno);
		*flags |= bit;
	}
	words_destroy(&words);
	lineno++;
	return (ret);
}

int			ensure_arguments(char **args, int required, int lineno)
{
	int	count;

	if (args == NULL)
		return (!put_error_line(ERR_LINEREAD, lineno));
	count = 0;
	while (count < required)
	{
		if (args[count] == NULL)
			return (!put_error_line(ERR_FEWARGS, lineno));
		count++;
	}
	if (args[count] != NULL)
		return (!put_error_line(ERR_MANYARGS, lineno));
	return (1);
}

void		words_destroy(char ***words)
{
	size_t	count;

	if (words == NULL || *words == NULL)
		return ;
	count = 0;
	while ((*words)[count] != NULL)
	{
		free((*words)[count]);
		count++;
	}
	free(*words);
	*words = NULL;
}
