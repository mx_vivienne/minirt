/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   light.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/13 20:18:14 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/13 20:18:24 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int		l_put(t_light l, t_color c, t_scene *sc)
{
	t_obj	o;
	t_light	*lt;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	lt = malloc(sizeof(t_light));
	if (lt == NULL)
		return (!put_error(ERR_ALLOC));
	*lt = l;
	o.type = OBJ_LIGHT;
	o.color = c;
	o.obj = (void*)lt;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

int		l_read(char **words, t_scene *sc, int lineno)
{
	t_light	l;
	t_color	c;
	double	intensity;

	if (!ensure_arguments(words, 4, lineno) ||
		!v_read(&(l.pos), words[1], lineno, VECMODE_ANY) ||
		!read_double(&intensity, words[2], lineno,
					DBLMODE_POS | DBLMODE_UNIT) ||
		!c_read(&c, words[3], lineno) ||
		!l_put(l, c_shade(c, intensity), sc))
		return (0);
	return (1);
}

t_color	l_trace(t_scene sc, t_hit hit, t_obj light)
{
	t_ray	shadow;
	t_hit	block;
	t_light	l;
	double	angle;

	(void)sc;
	l = *(t_light*)light.obj;
	shadow = r_through(hit.point, l.pos);
	shadow.origin = v_add(shadow.origin, v_factor(shadow.dir, EPSILON));
	angle = v_scalar(v_unit(hit.normal), v_unit(shadow.dir));
	if (angle <= 0.0)
		return ((t_color){0, 0, 0});
	block = r_trace(sc.objs, shadow, 0, 1);
	if (block.dist > 0 && block.dist < v_len(shadow.dir))
		return ((t_color){0, 0, 0});
	return (c_shade(c_mult(hit.obj.color, light.color), angle));
}

t_color	lighting(t_scene sc, t_hit hit)
{
	t_color	sum;
	size_t	offset;

	offset = 0;
	sum = (t_color){0, 0, 0};
	while (offset < sc.objs.len)
	{
		if (sc.objs.arr[offset].type == OBJ_LIGHT)
			sum = c_add(sum, l_trace(sc, hit, sc.objs.arr[offset]));
		offset++;
	}
	return (sum);
}
