/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   square.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/20 01:10:08 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/20 01:10:09 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int		sq_put(t_square sq, t_color c, t_scene *sc)
{
	t_obj		o;
	t_square	*s;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	s = malloc(sizeof(t_square));
	if (s == NULL)
		return (!put_error(ERR_ALLOC));
	sq.up = (t_vect){0, 0, -1};
	if (fabs(v_scalar(sq.up, sq.normal)) > 1.0 - EPSILON)
		sq.up = (t_vect){0, 1, 0};
	*s = sq;
	o.type = OBJ_SQUARE;
	o.color = c;
	o.obj = (void*)s;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

double	sq_ray(t_ray r, t_square sq)
{
	double	divisor;
	double	d;
	t_vect	up;
	t_vect	right;
	t_vect	point;

	divisor = v_scalar(r.dir, sq.normal);
	if (fabs(divisor) < EPSILON)
		return (-1.0);
	d = v_scalar(v_subt(sq.center, r.origin), sq.normal) / divisor;
	if (d < 0.0)
		return (-1.0);
	point = v_subt(r_point(r, d), sq.center);
	right = v_unit(v_cross(sq.normal, sq.up));
	up = v_cross(right, sq.normal);
	if (fabs(v_scalar(point, right)) <= sq.size / 2 &&
		fabs(v_scalar(point, up)) <= sq.size / 2)
		return (d);
	return (-1.0);
}

int		sq_read(char **words, t_scene *sc, int lineno)
{
	t_square	sq;
	t_color		c;

	if (ensure_arguments(words, 5, lineno) &&
		v_read(&(sq.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(sq.normal), words[2], lineno, VECMODE_NORM) &&
		read_double(&(sq.size), words[3], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		c_read(&c, words[4], lineno) &&
		sq_put(sq, c, sc))
		return (1);
	return (0);
}
