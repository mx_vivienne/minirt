/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   defines.h                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/13 19:04:26 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/13 19:04:28 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINES_H
# define DEFINES_H

# ifndef NAME
#  define NAME "miniRT"
# endif

# ifndef COMPILE_FOR_MAC
#  define COMPILE_FOR_MAC 0
# endif

# define OBJ_SPHERE		28787
# define OBJ_CYLINDER	31075
# define OBJ_PLANE		27760
# define OBJ_SQUARE		29043
# define OBJ_TRIANGLE	29300
# define OBJ_CIRCLE		26979
# define OBJ_LIGHT		108
# define OBJ_CAMERA		99
# define OBJ_RESOLUTION	82
# define OBJ_AMBIENT	65
# define OBJ_NONE		0

# define TYPE_FLAGS		2
# define FLAG_RES		1
# define FLAG_AMB		2

# define INTMODE_ANY	0
# define INTMODE_POS	1
# define INTMODE_NOZERO	2
# define INTMODE_ANGLE	4
# define INTMODE_BYTE	8

# define DBLMODE_ANY	0
# define DBLMODE_POS	1
# define DBLMODE_NOZERO	2
# define DBLMODE_UNIT	4

# if COMPILE_FOR_MAC
#  define KEY_LEFT		123
#  define KEY_RIGHT		124
#  define KEY_ESC		53
#  define EVT_CLOSE		17
#  define EVT_VIS		15
#  define MASK_VIS		65536L
# else
#  define KEY_LEFT		0xff51
#  define KEY_RIGHT		0xff53
#  define KEY_ESC		0xff1b
#  define EVT_CLOSE		33
#  define EVT_VIS		15
#  define MASK_VIS		65536L
# endif

# define VECMODE_ANY	0
# define VECMODE_NORM	16
# define EPSILON		0.0000001f
# define OBJS_BUFFER	32

#endif
