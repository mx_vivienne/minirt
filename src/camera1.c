/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   camera1.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/10 18:30:14 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/10 20:17:26 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static t_vect	cam_calc_vectors(t_camera c, t_pixbuf img,
								t_vect *down, t_vect *right)
{
	double	v;
	double	h;

	h = 2 * tan(c.fov / 360.0 * M_PI);
	v = h / img.w * img.h;
	*right = v_unit(v_cross(c.r.dir, c.vup));
	*down = v_factor(v_cross(c.r.dir, *right), v);
	*right = v_factor(*right, h);
	return (v_add4(c.r.origin, c.r.dir, v_div(*down, -2), v_div(*right, -2)));
}

static void		cam_render_loop(t_camera c, t_scene *sc, t_pixbuf img)
{
	t_vect	corner;
	size_t	x;
	size_t	y;
	t_vect	down;
	t_vect	right;

	corner = cam_calc_vectors(c, img, &down, &right);
	y = 0;
	while (y < img.h)
	{
		x = 0;
		while (x < img.w)
		{
			img.data[y * img.w + x] = r_color(*sc, r_through(c.r.origin,
				v_add3(corner, v_factor(right, (x + .5) / img.w),
					v_factor(down, (y + .5) / img.h))));
			x++;
		}
		y++;
	}
}

t_pixbuf		cam_render(t_camera c, t_scene *sc)
{
	t_pixbuf	img;

	(void)c;
	img = (t_pixbuf){sc->x, sc->y, NULL};
	img.data = malloc(img.w * img.h * sizeof(t_color));
	if (img.data == NULL)
	{
		put_error(ERR_ALLOC);
		return (img);
	}
	cam_render_loop(c, sc, img);
	return (img);
}

int				cam_put(t_camera c, t_scene *sc)
{
	t_obj		o;
	t_camera	*cam;

	if (!ensure_arr_size(&(sc->cams)))
		return (0);
	cam = malloc(sizeof(t_camera));
	if (cam == NULL)
		return (!put_error(ERR_ALLOC));
	c.vup = (t_vect){0, 1, 0};
	if (fabs(v_scalar(c.vup, c.r.dir)) > 1.0 - EPSILON)
		c.vup = (t_vect){0, 0, -1};
	*cam = c;
	o.type = OBJ_CAMERA;
	o.color = (t_color){0, 0, 0};
	o.obj = (void*)cam;
	sc->cams.arr[sc->cams.len] = o;
	sc->cams.len += 1;
	return (1);
}

int				cam_read(char **words, t_scene *sc, int lineno)
{
	t_camera c;

	if (ensure_arguments(words, 4, lineno) &&
		v_read(&(c.r.origin), words[1], lineno, VECMODE_ANY) &&
		v_read(&(c.r.dir), words[2], lineno, VECMODE_NORM) &&
		read_uint(&(c.fov), words[3], lineno, INTMODE_ANGLE) &&
		cam_put(c, sc))
		return (1);
	return (0);
}
