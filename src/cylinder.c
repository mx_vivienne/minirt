/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   cylinder.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/21 18:40:09 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/21 18:40:11 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static t_matrix	cy_affine_matrix(t_cyl cy)
{
	t_matrix	mtx;

	(void)cy;
	mtx = rotation_matrix(cy.up, (t_vect){0, 1, 0});
	mtx = m_mult(
		stretch_matrix((t_vect){1 / cy.radius, 1 / cy.size, 1 / cy.radius}),
		mtx);
	return (mtx);
}

int				cy_put(t_cyl cy, t_color c, t_scene *sc)
{
	t_obj	o;
	t_cyl	*cyl;

	if (!ensure_arr_size(&(sc->objs)))
		return (0);
	cyl = malloc(sizeof(t_cyl));
	if (cyl == NULL)
		return (!put_error(ERR_ALLOC));
	cy.radius /= 2;
	cy.size /= 2;
	cy.mtx = cy_affine_matrix(cy);
	*cyl = cy;
	o.type = OBJ_CYLINDER;
	o.color = c;
	o.obj = (void*)cyl;
	sc->objs.arr[sc->objs.len] = o;
	sc->objs.len += 1;
	return (1);
}

static double	cy_test_hits(t_ray r, double a, double b)
{
	if (fabs(r_point(r, a).y) > 1.0)
		a = -1.0;
	if (fabs(r_point(r, b).y) > 1.0)
		b = -1.0;
	return (min_pos(a, b));
}

double			cy_ray(t_ray r, t_cyl cy)
{
	t_vect	f;
	double	disc;

	(void)cy;
	r = (t_ray){m_apply(cy.mtx, v_subt(r.origin, cy.center)),
		m_apply(cy.mtx, r.dir)};
	f.x = r.dir.x * r.dir.x + r.dir.z * r.dir.z;
	f.y = 2 * (r.origin.x * r.dir.x + r.origin.z * r.dir.z);
	f.z = r.origin.x * r.origin.x + r.origin.z * r.origin.z - 1.0;
	disc = f.y * f.y - 4.0 * f.x * f.z;
	if (disc < 0)
		return (-1.0);
	disc = sqrt(disc);
	f.x *= 2;
	return (cy_test_hits(r, (-f.y + disc) / f.x, (-f.y - disc) / f.x));
}

int				cy_read(char **words, t_scene *sc, int lineno)
{
	t_cyl	cy;
	t_color	c;

	if (ensure_arguments(words, 6, lineno) &&
		v_read(&(cy.center), words[1], lineno, VECMODE_ANY) &&
		v_read(&(cy.up), words[2], lineno, VECMODE_NORM) &&
		read_double(&(cy.radius), words[3], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		read_double(&(cy.size), words[4], lineno,
					DBLMODE_POS | DBLMODE_NOZERO) &&
		c_read(&c, words[5], lineno) &&
		cy_put(cy, c, sc))
		return (1);
	return (0);
}
