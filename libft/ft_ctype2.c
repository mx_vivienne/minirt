/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ctype2.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/07 22:28:37 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/07 22:51:27 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_isupper(int c)
{
	return (c >= 'A' && c <= 'Z');
}

int		ft_islower(int c)
{
	return (c >= 'a' && c <= 'z');
}

int		ft_isspace(int c)
{
	return (c == ' ' || c == '\n' || c == '\t' ||
			c == '\f' || c == '\r' || c == '\v');
}

int		ft_ispunct(int c)
{
	return (' ' < c && c <= '~' &&
			(c < 'a' || 'z' < c) &&
			(c < 'A' || 'Z' < c) &&
			(c < '0' || '9' < c));
}

int		ft_isxdigit(int c)
{
	return (('0' <= c && c <= '9') || ('a' <= c && c <= 'f') ||
			('A' <= c && c <= 'F'));
}
