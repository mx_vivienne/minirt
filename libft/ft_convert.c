/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_convert.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/30 01:46:41 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/07 23:30:19 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_toupper(int c)
{
	if ('a' <= c && c <= 'z')
		return (c - 32);
	return (c);
}

int			ft_tolower(int c)
{
	if ('A' <= c && c <= 'Z')
		return (c + 32);
	return (c);
}

int			ft_atoi(const char *n)
{
	size_t			i;
	int				sign;
	unsigned int	val;

	i = 0;
	val = 0;
	sign = 1;
	while (ft_isspace(n[i]))
		i++;
	if (n[i] == '+' || n[i] == '-')
	{
		if (n[i] == '-')
			sign = -1;
		i++;
	}
	while (ft_isdigit(n[i]))
	{
		val = 10 * val + n[i] - '0';
		i++;
	}
	return ((int)(sign * val));
}

char		*ft_itoa(int n)
{
	char	*str;
	long	abs;
	size_t	offset;

	str = (char*)ft_calloc(12, sizeof(char));
	if (str != NULL)
	{
		if (n < 0)
			abs = -1 * (long)n;
		else
			abs = (long)n;
		offset = 0;
		while (abs > 0 || offset == 0)
		{
			str[offset] = (char)(abs % 10) + '0';
			abs /= 10;
			offset++;
		}
		if (n < 0)
			str[offset] = '-';
		ft_strrev(str);
	}
	return (str);
}

double		ft_atod(const char *f)
{
	double	integ;
	double	frac;
	int		sign;
	size_t	i;

	i = 0;
	frac = 0.0;
	integ = 0.0;
	while (ft_isspace(f[i]))
		i++;
	sign = -2 * (f[i] == '-') + 1;
	i += (f[i] == '+') || (f[i] == '-');
	while (ft_isdigit(f[i]))
	{
		integ = integ * 10 + (f[i] - '0');
		i++;
	}
	i += f[i] == '.';
	while (ft_isdigit(f[i]))
	{
		frac = frac / 10 + (f[i] - '0');
		i++;
	}
	return (sign * (integ + frac / 10));
}
