/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   libft.h                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/28 13:49:20 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/07 23:30:20 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>

# ifndef GNL_BUFFER_SIZE
#  define GNL_BUFFER_SIZE 32
# endif

# ifndef GNL_OPEN_MAX
#  define GNL_OPEN_MAX 16
# endif

typedef struct		s_list
{
	void			*content;
	struct s_list	*next;
}					t_list;

typedef struct		s_buf {
	char			*remains;
	int				fd;
}					t_buf;

/*
** ft_ctype1.c
*/
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);

/*
** ft_ctype2.c
*/

int					ft_isupper(int c);
int					ft_islower(int c);
int					ft_isspace(int c);
int					ft_ispunct(int c);
int					ft_isxdigit(int c);

/*
** ft_convert.c
*/
int					ft_tolower(int c);
int					ft_toupper(int c);
int					ft_atoi(const char *n);
char				*ft_itoa(int n);
double				ft_atod(const char *f);

/*
** ft_str1.c
*/
size_t				ft_strlen(const char *s);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strnstr(const char *big, const char *little,
								size_t len);

/*
** ft_str2.c
*/
char				*ft_strdup(const char *s);
size_t				ft_strlcpy(char *dst, const char *src, size_t dstsize);
size_t				ft_strlcat(char *dst, const char *src, size_t dstsize);
char				*ft_substr(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);

/*
** ft_str3.c
*/
int					ft_isinset(const char *set, char c);
void				ft_strrev(char *str);
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strtrim(char const *s1, char const *set);

/*
** ft_split.c
*/
char				**ft_split(char const *s, char c);

/*
** ft_gnl.c
*/
int					get_next_line(int fd, char **line);
size_t				find_char(char *str, char c);
int					increase_size(char **str, size_t *size, size_t extra);

/*
** ft_mem1.c
*/
void				*ft_memset(void *s, int c, size_t n);
void				ft_bzero(void *s, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);

/*
** ft_mem2.c
*/
void				*ft_memcpy(void *dest, const void *src, size_t n);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t n);
void				*ft_calloc(size_t nmemb, size_t size);

/*
** ft_put.c
*/
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char *s, int fd);
void				ft_putendl_fd(char *s, int fd);
void				ft_putnbr_fd(int n, int fd);

/*
** ft_list1.c
*/
t_list				*ft_lstnew(void *content);
int					ft_lstsize(t_list *lst);
t_list				*ft_lstlast(t_list *lst);

/*
** ft_list2.c
*/
void				ft_lstadd_front(t_list **lst, t_list *new);
void				ft_lstadd_back(t_list **lst, t_list *new);
void				ft_lstdelone(t_list *lst, void (*del)(void*));
void				ft_lstclear(t_list **lst, void (*del)(void*));

/*
** ft_list3.c
*/
void				ft_lstiter(t_list *lst, void (*f)(void*));
t_list				*ft_lstmap(t_list *lst, void *(*f)(void *),
								void (*del)(void *));

#endif
