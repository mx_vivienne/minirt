/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_gnl.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/11 13:54:06 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/04 21:32:46 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	exit_loop(char remains[], int read_status, int linesize)
{
	if (read_status == 0)
	{
		remains[0] = '\0';
		return (linesize > 0);
	}
	return (-1);
}

static int	gnl_loop(char **line, t_buf buf,
		size_t linemem, size_t linesize)
{
	size_t	breakpos;
	ssize_t	read_status;

	while (1)
	{
		breakpos = find_char(*line, '\n');
		if ((*line)[breakpos] == '\n')
		{
			(*line)[breakpos] = '\0';
			ft_strlcpy(buf.remains, *line + breakpos + 1, GNL_BUFFER_SIZE);
			return (1);
		}
		if (!increase_size(line, &linemem,
					GNL_BUFFER_SIZE - (linemem - linesize - 1)))
		{
			free(*line);
			return (-1);
		}
		read_status = read(buf.fd, *line + (linesize), GNL_BUFFER_SIZE);
		if (read_status <= 0)
			return (exit_loop(buf.remains, read_status, linesize));
		linesize += (size_t)read_status;
		(*line)[linesize] = '\0';
	}
}

int			get_next_line(int fd, char **line)
{
	static char	remains[GNL_OPEN_MAX][GNL_BUFFER_SIZE];
	char		*curr_buf;
	size_t		linemem;
	size_t		linesize;

	if (GNL_BUFFER_SIZE <= 0 || fd >= GNL_OPEN_MAX)
		return (-1);
	curr_buf = remains[fd];
	linesize = ft_strlen(curr_buf);
	linemem = 1 + linesize;
	*line = (char*)malloc(linemem);
	if (*line == NULL)
		return (-1);
	ft_strlcpy(*line, curr_buf, linemem);
	return (gnl_loop(line, (t_buf){curr_buf, fd}, linemem, linesize));
}

size_t		find_char(char *str, char c)
{
	size_t	offset;

	offset = 0;
	while (str[offset] != '\0')
	{
		if (str[offset] == c)
			break ;
		offset++;
	}
	return (offset);
}

int			increase_size(char **str, size_t *curr_size, size_t extra)
{
	char	*new;

	new = (char*)malloc(*curr_size + extra);
	if (new == NULL)
		return (0);
	new[0] = '\0';
	ft_strlcat(new, *str, *curr_size + extra);
	free(*str);
	*str = new;
	*curr_size += extra;
	return (1);
}
