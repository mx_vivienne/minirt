/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mem2.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/29 19:40:44 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/18 16:44:40 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	*memcpy_ltr(void *dest, const void *src, size_t n)
{
	size_t			offset;
	unsigned char	*destmem;
	unsigned char	*srcmem;

	offset = 0;
	destmem = (unsigned char*)dest;
	srcmem = (unsigned char*)src;
	while (offset < n)
	{
		destmem[offset] = srcmem[offset];
		offset++;
	}
	return (dest);
}

void		*ft_memcpy(void *dest, const void *src, size_t n)
{
	if (dest == NULL && src == NULL)
		return (NULL);
	return (memcpy_ltr(dest, src, n));
}

void		*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t			offset;
	unsigned char	*destmem;
	unsigned char	*srcmem;

	offset = 0;
	destmem = (unsigned char*)dest;
	srcmem = (unsigned char*)src;
	while (offset < n)
	{
		destmem[offset] = srcmem[offset];
		if (destmem[offset] == (unsigned char)c)
			return ((void*)(destmem + offset + 1));
		offset++;
	}
	return (NULL);
}

void		*ft_memmove(void *dest, const void *src, size_t n)
{
	size_t			offset;
	unsigned char	*destmem;
	unsigned char	*srcmem;

	if (dest == src || n == 0)
		return (dest);
	if (dest < src)
		return (memcpy_ltr(dest, src, n));
	offset = n - 1;
	destmem = (unsigned char*)dest;
	srcmem = (unsigned char*)src;
	while (1)
	{
		destmem[offset] = srcmem[offset];
		if (0 == offset)
			return (dest);
		offset--;
	}
}

void		*ft_calloc(size_t nmemb, size_t size)
{
	size_t	range;
	char	*mem;

	range = nmemb * size;
	if (nmemb != 0 && size != 0 && range / nmemb != size)
		return (NULL);
	mem = malloc(range);
	if (mem != NULL)
		ft_bzero((void*)mem, range);
	return ((void*)mem);
}
