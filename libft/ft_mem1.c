/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mem1.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/29 02:28:25 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/18 16:40:14 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *s, int c, size_t n)
{
	char	*ptr;
	size_t	offset;

	offset = 0;
	ptr = (char*)s;
	while (offset < n)
	{
		ptr[offset] = (char)c;
		offset++;
	}
	return (s);
}

void	ft_bzero(void *s, size_t n)
{
	char		*cptr;
	long long	*lptr;
	size_t		counter;

	lptr = (long long*)s;
	counter = n / sizeof(long long);
	while (0 < counter)
	{
		*lptr = 0;
		lptr++;
		counter--;
	}
	cptr = (char*)lptr;
	counter = n % sizeof(long long);
	while (0 < counter)
	{
		*cptr = 0;
		cptr++;
		counter--;
	}
}

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	byte;
	unsigned char	*mem;
	size_t			offset;

	byte = (unsigned char)c;
	mem = (unsigned char*)s;
	offset = 0;
	while (offset < n)
	{
		if (byte == mem[offset])
			return (mem + offset);
		offset++;
	}
	return (NULL);
}

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t			offset;
	unsigned char	*m1;
	unsigned char	*m2;

	offset = 0;
	m1 = (unsigned char*)s1;
	m2 = (unsigned char*)s2;
	while (offset < n)
	{
		if (m1[offset] != m2[offset])
			return (m1[offset] - m2[offset]);
		offset++;
	}
	return (0);
}
