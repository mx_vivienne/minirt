/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_list2.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/31 01:39:56 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/18 16:11:39 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_front(t_list **lst, t_list *new)
{
	if (lst == NULL || new == NULL)
		return ;
	new->next = *lst;
	*lst = new;
}

void	ft_lstadd_back(t_list **lst, t_list *new)
{
	t_list	*end;

	if (lst == NULL || new == NULL)
		return ;
	if (*lst == NULL)
		*lst = new;
	else
	{
		end = ft_lstlast(*lst);
		if (end == NULL)
			return ;
		end->next = new;
	}
}

void	ft_lstdelone(t_list *lst, void (*del)(void*))
{
	if (lst == NULL)
		return ;
	if (lst->content != NULL && del != NULL)
		del(lst->content);
	free(lst);
}

void	ft_lstclear(t_list **lst, void (*del)(void*))
{
	t_list	*next;

	if (lst == NULL)
		return ;
	if ((*lst)->next != NULL)
	{
		next = (*lst)->next;
		ft_lstclear(&next, del);
	}
	ft_lstdelone(*lst, del);
	*lst = NULL;
}
