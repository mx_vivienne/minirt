/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_str2.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/29 21:11:34 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/20 17:02:03 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	char	*dup;
	size_t	offset;

	dup = (char*)malloc(ft_strlen(s) + 1);
	if (NULL == dup)
		return (NULL);
	offset = 0;
	while (s[offset] != '\0')
	{
		dup[offset] = s[offset];
		offset++;
	}
	dup[offset] = '\0';
	return (dup);
}

size_t	ft_strlcpy(char *dst, const char *src, size_t dstsize)
{
	size_t	offset;

	offset = 0;
	while (offset + 1 < dstsize)
	{
		if (src[offset] == '\0')
			break ;
		dst[offset] = src[offset];
		offset++;
	}
	if (dstsize > 0)
		dst[offset] = '\0';
	while (src[offset] != '\0')
		offset++;
	return (offset);
}

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t offset;

	offset = 0;
	while (dst[offset] != '\0' && offset < dstsize)
		offset++;
	if (offset < dstsize)
		return (offset + ft_strlcpy(dst + offset, src, dstsize - offset));
	return (offset + ft_strlcpy(dst + offset, src, 0));
}

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*substr;

	if (s == NULL)
		return (NULL);
	substr = (char*)malloc(len + 1);
	if (substr == NULL)
		return (NULL);
	if (start > ft_strlen(s))
		substr[0] = '\0';
	else
		ft_strlcpy(substr, s + start, len + 1);
	return (substr);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	size;
	char	*joined;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	size = ft_strlen(s1) + ft_strlen(s2) + 1;
	joined = (char*)malloc(size);
	if (joined == NULL)
		return (NULL);
	ft_strlcpy(joined, s1, size);
	ft_strlcat(joined, s2, size);
	return (joined);
}
