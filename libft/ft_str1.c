/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_str1.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/29 01:36:04 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/18 16:50:38 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlen(const char *s)
{
	size_t	offset;

	offset = 0;
	while (s[offset] != '\0')
		offset++;
	return (offset);
}

char	*ft_strchr(const char *s, int c)
{
	size_t offset;

	offset = 0;
	while (s[offset] != c && s[offset] != '\0')
		offset++;
	if (s[offset] == c)
		return ((char*)s + offset);
	return (NULL);
}

char	*ft_strrchr(const char *s, int c)
{
	size_t	offset;
	char	*chr;

	offset = 0;
	chr = NULL;
	while (1)
	{
		if (s[offset] == c)
			chr = (char*)s + offset;
		if (s[offset] == '\0')
			return (chr);
		offset++;
	}
}

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	offset;

	offset = 0;
	while (offset < n)
	{
		if (s1[offset] != s2[offset])
			return ((unsigned char)s1[offset] - (unsigned char)s2[offset]);
		if (s1[offset] == '\0')
			break ;
		offset++;
	}
	return (0);
}

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	boffset;
	size_t	loffset;
	size_t	substr;

	boffset = 0;
	loffset = 0;
	substr = 0;
	while (little[loffset] != '\0')
	{
		if (big[boffset] == '\0' || boffset >= len)
			return (NULL);
		if (big[boffset] == little[loffset])
		{
			boffset++;
			loffset++;
		}
		else
		{
			substr++;
			boffset = substr;
			loffset = 0;
		}
	}
	return ((char*)big + substr);
}
