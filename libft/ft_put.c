/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_put.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/28 17:24:39 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/18 17:00:25 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}

void		ft_putstr_fd(char *s, int fd)
{
	if (s == NULL)
		return ;
	write(fd, s, ft_strlen(s));
}

void		ft_putendl_fd(char *s, int fd)
{
	if (s == NULL)
		return ;
	write(fd, s, ft_strlen(s));
	write(fd, "\n", 1);
}

static void	ft_putuint_fd(unsigned int n, int fd)
{
	char	digit;

	if (n > 9)
		ft_putuint_fd(n / 10, fd);
	digit = '0' + (char)(n % 10);
	write(fd, &digit, 1);
}

void		ft_putnbr_fd(int n, int fd)
{
	if (n < 0)
	{
		write(fd, "-", 1);
		ft_putuint_fd((unsigned int)(-1 * n), fd);
	}
	else
		ft_putuint_fd((unsigned int)n, fd);
}
