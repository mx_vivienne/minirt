NAME := miniRT
DBG_NAME := debugRT
OS := $(shell uname)
PWD := $(shell pwd)
$(info detected OS: $(OS))

BONUSDIR := bonus

# source files and header files
ifndef USE_BONUS
  IDIR := src
  SDIR := src
else
  IDIR := src/$(BONUSDIR)
  SDIR := src/$(BONUSDIR)
endif

# hidden directories for .o files
ODIR := $(SDIR)/.obj
DDIR := $(SDIR)/.debug

# libft
FTDIR := libft
LIBFT := $(FTDIR)/libft.a

# libmlx
ifeq ($(OS),Linux)
  MLXDIR := libmlx_linux
  MLXNAME := libmlx.a
  LIBMLX := $(MLXDIR)/$(MLXNAME)
else
  MLXDIR := libmlx
  MLXNAME := libmlx.dylib
  LIBMLX := $(MLXDIR)/$(MLXNAME)
endif

ifndef USE_BONUS
  _INC := minirt.h typedefs.h defines.h errors.h
  SRC := bmp.c camera1.c camera2.c circle.c color.c cylinder.c error.c\
         input1.c input2.c input3.c light.c matrix1.c matrix2.c minirt.c\
         misc1.c misc2.c misc3.c objs.c plane.c ray.c sphere.c square.c\
         triangle.c vector1.c vector2.c vector3.c window.c
else
  _INC := minirt.h typedefs.h defines.h keycodes.h errors.h
  _INC := $(_INC:.h=_bonus.h)
  SRC := bmp.c camera1.c camera2.c camera3.c circle.c color1.c color2.c\
         cone1.c cone2.c cube.c cylinder1.c cylinder2.c error.c filter.c\
         input1.c input2.c input3.c input4.c light1.c light2.c\
         material.c material_color.c material_surface.c matrix1.c matrix2.c\
         minirt.c misc1.c misc2.c misc3.c misc4.c pyramid.c rainbow.c\
         objs1.c objs2.c pgm.c plane.c ppm.c ray.c skybox.c sphere.c square.c\
         threads.c triangle1.c triangle2.c\
         vector1.c vector2.c vector3.c vector4.c window.c
  SRC := $(SRC:.c=_bonus.c)
endif

# prepend directories to filenames
_OBJ := $(SRC:.c=.o)
OBJ := $(patsubst %,$(ODIR)/%,$(_OBJ))
DBG := $(patsubst %,$(DDIR)/%,$(_OBJ))
INC := $(patsubst %,$(IDIR)/%,$(_INC))

CFLAGS := -Wall -Werror -Wextra -g -I$(FTDIR) -I$(MLXDIR) -I$(IDIR)
DBGFLAGS := -fsanitize=address

ifeq ($(OS),Linux)
  LDFLAGS := -lm -lXext -lX11
else
  LDFLAGS := -lm -framework OpenGL -framework AppKit
  CFLAGS += -D COMPILE_FOR_MAC=1
endif
ifdef USE_BONUS
  CFLAGS += -pthread
endif


# regular rules
$(NAME): $(OBJ) $(LIBFT) $(MLXNAME) $(INC)
	$(CC) $(CFLAGS) -DNAME=\"$(NAME)\" -o $@ \
	$(OBJ) $(LIBFT) $(LIBMLX) $(LDFLAGS)

$(ODIR)/%.o: $(SDIR)/%.c $(INC)
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -DNAME=\"$(NAME)\" -c -o $@ $<

# debug rules
$(DBG_NAME): $(DBG) $(LIBFT) $(MLXNAME) $(INC)
	$(CC) $(CFLAGS) -DNAME=\"$(DBG_NAME)\" $(DBGFLAGS) -o $@ \
	$(DBG) $(LIBFT) $(LIBMLX) $(LDFLAGS)

$(DDIR)/%.o: $(SDIR)/%.c $(INC)
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -DNAME=\"$(DBG_NAME)\" $(DBGFLAGS) -c -o $@ $<


$(LIBFT):
	make -C $(FTDIR) all

$(MLXNAME):
	make -C $(MLXDIR) all
	cp $(LIBMLX) $(MLXNAME)


.PHONY: all bonus debug bonus_debug re clean fclean

all: $(NAME)

bonus:
	make USE_BONUS=1 all

bonus_debug:
	make USE_BONUS=1 debug

debug: $(DBG_NAME)

re: fclean all

clean:
	-rm -f $(OBJ) $(DBG)
ifneq ($(USE_BONUS),1)
	make USE_BONUS=1 clean
endif
	make -C $(FTDIR) clean
	make -C $(MLXDIR) clean

fclean:
	-rm -f $(NAME) $(DBG_NAME) $(OBJ) $(DBG) $(MLXNAME)
ifneq ($(USE_BONUS),1)
	make USE_BONUS=1 fclean
endif
	make -C $(FTDIR) fclean
	make -C $(MLXDIR) clean
