MiniRT is a simple raytracer, rendering scenes read from a .rt file either to
a window or to .bmp images. These files consist of multiple lines of instructions,
described in detail below. It uses the included minilibx library for window
management.

MiniRT is split up into two versions, one limited to features mandatory
to the project and one including optional bonus features. Compile with `make all`
and `make bonus` respectively.

# mandatory features:

+ 5 objects: planes, spheres, cylinders, squares and triangles\
`pl (position) (orientation) (color)`\
`sp (position) (diameter) (color)`\
`cy (position) (orientation) (diameter) (height] (color)`\
`sq (position) (orientation) (side length) (color)`\
`tr (position) (point) (point) (point) (color)`

+ resolution:\
`R (width) (height)`

+ ambient lighting:\
`A (intensity) (color)`

+ cameras:\
`c (position) (direction) (horizontal fov, degrees)`

+ spot lights:\
`l (position) (intensity) (color)`

# bonus features:

+ comments: the first '#' in a line and everything after it is ignored

+ ppm images for textures (paths relative to the executable)

+ antialiasing:\
`AA (amount)`

+ threading:\
`TH (amount)`

+ materials! after declaring a material with a unique name, it can be used on
all following objects instead of a color\
`mat (name) (color) (surface)`

+ available color attributes: (scaling textures only supported for planes)\
`flat:(r,g,b)  (rgb)  checker:(r,g,b):(r,g,b)[:scale]`\
`rainbow:(r,g,b)  texture:path/to/img.ppm[:scale]`

+ available surface attributes: `regular  sine`

+ cameras now have an optional filter, as well as
optional red/cyan anaglyph 3d (eye distance 0 for usual 2d)\
`c (position) (direction) (fov)`\
`c (position) (direction) (fov) (eye distance)`\
`c (position) (direction) (fov) (eye distance) (filter keyword)`

+ available filters are:\
`none  sepia  greyscale  invert  scanline  hueshift[angle]`

+ interactive camera: the current camera can be moved/rotated:
(relative to the perspective of the camera)\
a/d: move on X axis (left/right)\
w/s: move on Y axis (up/down)\
w/a: move on Z axis (forward/back)\
1/2: rotate around X axis (up/down)\
3/4: rotate around Y axis (left/right)\
5/6: rotate around Z axis (tilt camera)

+ parallel lights, acting like a sun for the entire scene\
`l2 (direction) (intensity) (color)`

+ cylinders now have an optional argument "capped" that fills in the bases\
`cy (center) (up) (diameter) (height) (color)`\
`cy (center) (up) (diameter) (height) (color) capped`

+ circles, cubes, pyramids, double cones\
`ci (center) (normal) (radius) (color)`\
`cb (center) (up) (right) (side length) (color)`\
`py (base center) (up) (right) (base size) (height) (color)`\
`co (center) (up) (base diameter) (height) (color)`\
`co (center) (up) (base diameter) (height) (color) capped`

+ skyboxes, always centered on the origin, taking 6 materials for its sides
(note that skyboxes arent shaded and cannot cast shadows)\
`sky (center) (+x material) (-x mat) (+y mat) (-y mat) (+z mat) (-z mat)`
